package com.qf.mapper.product;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.entity.TbProduct;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface ProductMapper extends BaseMapper<TbProduct> {
    @Update("update tb_product set update_time =#{format} where product_id=#{pid}")
    int updateProductUpdateTime(@Param("format") String format, @Param("pid") int pid);
    @Update("update tb_product set create_time =#{format} where product_id=#{pid}")
    void creatTime(@Param("format") String format, @Param("pid") Integer productId);
    @Update("update tb_product set update_time =#{format} where product_id=#{pid}")
    void updateTime(@Param("format") String format, @Param("pid") Integer productId);
}