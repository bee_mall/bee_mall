package com.qf.mapper.skumapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.entity.TbProductSku;
import org.apache.ibatis.annotations.Mapper;

/**
 * ClassName: SkuMapper
 * Description:
 * date: 2023/6/12 11:19
 *
 * @author xiaoliu
 * @version 2021.2
 * @since JDK 1.8
 */
@Mapper
public interface SkuMapper extends BaseMapper<TbProductSku> {

}
