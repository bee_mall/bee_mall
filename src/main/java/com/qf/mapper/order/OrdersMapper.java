package com.qf.mapper.order;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.entity.TbOrders;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author h'j
 * @description TODO
 * @date 2023/6/10 23:31
 */
@Mapper
public interface OrdersMapper extends BaseMapper<TbOrders> {

    @Select("select * from tb_orders o, tb_users u where o.user_id=u.user_id and username like #{userName}")
    List<TbOrders> selectOrders(String userName);

}
