package com.qf.mapper.order;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.entity.TbOrderItem;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author h'j
 * @description TODO
 * @date 2023/6/11 9:00
 */
@Mapper
public interface OrderItemMapper extends BaseMapper<TbOrderItem> {

}
