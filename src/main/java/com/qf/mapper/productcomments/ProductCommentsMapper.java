package com.qf.mapper.productcomments;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.entity.TbProductComments;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 作者：涛
 * 时间：2023/6/10 16:10
 * 描述：商品评价 Mapper 接口
 */
@Mapper
public interface ProductCommentsMapper extends BaseMapper<TbProductComments> {
}


