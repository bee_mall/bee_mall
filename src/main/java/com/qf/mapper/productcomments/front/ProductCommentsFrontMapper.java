package com.qf.mapper.productcomments.front;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.entity.productcommentsvo.ProductCommentsVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 作者：涛
 * 时间：2023/6/13 14:39
 * 描述：
 */
@Mapper
@Repository
public interface ProductCommentsFrontMapper extends BaseMapper<ProductCommentsVO> {



}
