package com.qf.mapper.productDetail;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.entity.ProductDetails.TbProductVo;
import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.web.bind.annotation.DeleteMapping;

/**
 * ClassName: productVoMapper
 * Description:
 * date: 2023/6/13 10:43
 *
 * @author xiaoliu
 * @version 2021.2
 * @since JDK 1.8
 */
@Mapper
public interface ProductVoMapper extends BaseMapper<TbProductVo> {
}

