package com.qf.mapper.productDetail;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.entity.ProductDetails.ShoppingCartVO;
import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.Mapper;

/**
 * ClassName: ShoppingCartMapper
 * Description:
 * date: 2023/6/14 18:08
 *
 * @author xiaoliu
 * @version 2021.2
 * @since JDK 1.8
 */
@Mapper
public interface ShoppingDetailsCartMapper extends BaseMapper<ShoppingCartVO> {
}
