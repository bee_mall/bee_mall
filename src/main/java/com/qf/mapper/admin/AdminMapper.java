package com.qf.mapper.admin;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.entity.TbAdmin;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AdminMapper extends BaseMapper<TbAdmin> {
}
