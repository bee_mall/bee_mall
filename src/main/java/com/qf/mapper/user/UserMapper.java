package com.qf.mapper.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.entity.TbUsers;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface UserMapper extends BaseMapper<TbUsers> {
}
