package com.qf.mapper.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fasterxml.jackson.databind.ser.Serializers;
import com.qf.entity.TbUserAddr;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserAddrMapper extends BaseMapper<TbUserAddr> {
}
