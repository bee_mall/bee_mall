package com.qf.mapper.parammapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.entity.TbProductParams;
import org.apache.ibatis.annotations.Mapper;

/**
 * ClassName: ParamMapper
 * Description:
 * date: 2023/6/12 11:19
 *
 * @author xiaoliu
 * @version 2021.2
 * @since JDK 1.8
 */
@Mapper
public interface ParamMapper extends BaseMapper<TbProductParams> {
}
