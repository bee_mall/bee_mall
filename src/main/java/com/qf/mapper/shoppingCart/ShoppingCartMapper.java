package com.qf.mapper.shoppingCart;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.entity.TbShoppingCart;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author h'j
 * @description TODO
 * @date 2023/6/13 13:56
 */
@Mapper
public interface ShoppingCartMapper extends BaseMapper<TbShoppingCart> {


}
