package com.qf.mapper.dirstrictmapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.entity.TbDictDistrict;
import org.apache.ibatis.annotations.Mapper;

/**
 * ClassName: DirstrictMapper
 * Description:
 * date: 2023/6/10 16:32
 *
 * @author xiaoliu
 * @version 2021.2
 * @since JDK 1.8
 */
@Mapper
public interface DirstrictMapper extends BaseMapper<TbDictDistrict> {
}

