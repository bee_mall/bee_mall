package com.qf.mapper.category;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.entity.TbCategory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CategoryMapper extends BaseMapper<TbCategory> {
    @Select("select * from tb_category")
    List<TbCategory> findAll();
}
