package com.qf.config;

import com.qf.admininterceptor.AdminLoginInterceptor;
import com.qf.userinterceptor.UserInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
@Configuration
public class WebInterceptionConfing implements WebMvcConfigurer {
    @Autowired
    AdminLoginInterceptor adminLoginInterceptor ;
    @Autowired
    UserInterceptor userInterceptor;
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedMethods("*")  //put post get put option...
                .allowedOrigins("http://localhost:8080")
                .allowCredentials(true)
                .allowedHeaders("*");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(adminLoginInterceptor).excludePathPatterns("/admin/login","/product/upload","/**/export");
        registry.addInterceptor(userInterceptor).excludePathPatterns("/user/userLogin","/user/userLogon","/user/logout","/**/find*/**");
    }

}