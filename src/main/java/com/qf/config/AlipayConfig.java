package com.qf.config;


public class AlipayConfig {
    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数
    public static String return_url = "http://47.109.77.57:8080/#/paySuccess";  // 此处使用外网ip(使用免费的花生壳或者netapp产生的外网域名)

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://47.109.77.57:8088/order/aliPayNotify";
    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id = "2021000122685103";

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    //public static String notify_url = "http://工程公网访问地址/alipay.trade.page.pay-JAVA-UTF-8/notify_url.jsp";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    //public static String return_url = "http://工程公网访问地址/alipay.trade.page.pay-JAVA-UTF-8/return_url.jsp";



    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key =

            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArowEFVmbvqPUhl1xfITTCESP0eeTDXPdQG8VfNPlIEEUnk8Q9gvH0HTLZiEwQLM0IfXZ83MspIO9+7eWwPC6sZiCdssamEv3ZSFWrZvZ0G95x+xoHI9wnBzpVS+mlhEUR1lZnbDWZgFflyScX6S9mHPUnV4azowi7jRKPis27B5Gqb2BYLpbT+EL1OzoeGWEAaFuw/0WoTiI6zhg0J97dAMHMshNCzpdJD0C3bo0qSb7Q5P/eKsQeIHst7E5NAsTwxBrvI25gJsqgSCUSUhENLMfooYk8PUS9jyyBhkm+lBT2Dg9CRSAc2PtK+FMqithn8fPfzmLtC4wq83X/ePLmwIDAQAB";

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key=

            "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCaP3rM/UhUwHVw821lHlkU8Ap1vfYz3Hp41rz2hEmodaBNcBryWsi1oAcBBV8eKrMiP68aMjHBPcHhp80q8BqyhUN21uYMlPNaONBSwmeeYdupjEAVtDh79PA+T0LWYGMYnYYAhegPhckgBPUwMitqu/9taklfVqAJbsj1h486z+UKYUv3V8If6CHugb5hLbKPjq/qPevNr42jb9Vdx0LJ6L5jJcTJE2U9kkKw3CcLapWCn19aytFN1bKuKz5Q7waBs8DATIP1bzzAYt4GCiv3oQskgrv4wxQo9rujle1dHMPWfJ2N607DTUdAvCs75YdvcXiNo3s25qZq3QXCnI+5AgMBAAECggEAFgNjOigXj4jb+d+zyA+ZtQRA6ebPYT1g+SfkVeYxQOpwkzyhi97q8AjDlyC2H28Gm6etDLcHJMX0iojRHgSgOsQMCcTHlhoX1NNw367A7PiYSc0rRC2xNfeR5cKYDlI5MkUg36g8oyJ7vqcWeudeDOCBppLZn9KUCmSWJ3xOM51JFtpARiqIHNpHOeZ67+ZTuCNN/88xMWMHiXvUst7sbnSku2onE74kTHFQi20eggyIu6KoD+dQnJISzbHJsbXwFpaxldEeXmwypWiEBubfbm4vty6h5gqJZTfC0loNrkaos8d0fOsf+FfyTsZa8Fs51vAGTBaIe/lPLw+Q3mF+RQKBgQDOg0SWGqMV1vwBOOi8nOCwtXiEKqT1wq0S5kcuzmObxRf3NU3hgwjYSFFL7KWFQF20lDtGcmyKinY422010ynijt3wqFfpRkqz3dPldw20UB4a3YyairGImvPBHqi3zV/yZZX/BtRINbz05FqT+Lam+eYLJOXI2Hfde4o8YPRAswKBgQC/Nfm6F9wNVN9s7rOPpaphOgxeTKJuboYwtsV0GKomzZs8RSqd1iSbSMziYdoCvHzi5bQqWuC2Rnz4sZR5N+b58nxAItRwN5I1QBSVb+VGbuQ89doZv2cmPAEPwYd4gG2Nd5M2ew6uNtrjvD5KTaliMVcPeBnIvpCjbTWAcIKL4wKBgGVwlYAmFXXk5GS1T51WcghbG5fQWrddqSZfdxfGyab6aYtE47yiYPXckBJ7a9LIiz7okTVJf4eN/ARxxHNsBmcDRoGwL36myIsRq8RLdq/bqu4HZcmD1zEC7Dkwli+y8i5fOA7Ni/jsbKTehp+ozA+5MQbIWRcPPdHNnVmLRm+BAoGABoPXS3DurYGbz0Fe2uEhKgjpTPk9e5fGeLhxh+AjRAkzZd1Rc2lNfAECAiClZfx4IN8EwYCxMWS7c1nRnkO6Wf/aHfFR3bT3BVt2mBTk8f5rRmx2DvSHljTrMFc+v2tY0D4nxO5ydJsn4CSU1sFIqTDXL46/Cij8EqVw8jxjJlMCgYEAtQ6G1vfRbu0nhf1P2cT/j6EKH245DKUWFGWAz3ctmzOz3PtIxO0s9gIkL0CGcX3mAOQA4hp4nx0gY6s6zNG7L+FddVL2FAsmRoUeCLCE28jF+GGHxichPUci8D8fCTo4LDBLq56foOtZ54tLrwhKcCMMvuph/xeDcRxlTI7iYqE=";

    // 签名方式
    public static String sign_type = "RSA2";
    // 字符编码格式
    public static String charset = "utf-8";
    // 支付宝网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do" ;
}

