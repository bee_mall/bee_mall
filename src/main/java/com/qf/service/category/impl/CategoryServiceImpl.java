package com.qf.service.category.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.entity.TbCategory;
import com.qf.entity.TbProduct;
import com.qf.mapper.category.CategoryMapper;
import com.qf.mapper.product.ProductMapper;
import com.qf.service.category.CategoryService;
import com.qf.util.ResultMsg;
import com.qf.util.product.QiNiuYunUploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
@Transactional
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, TbCategory> implements CategoryService {
    @Autowired
    CategoryMapper categoryMapper ;
    @Autowired
    ProductMapper productMapper ;
    @Override
    public ResultMsg findAll(int page, int limit) {
        Page<TbCategory> page1 =new Page<>(page,limit);
        Page<TbCategory> page2 = this.page(page1);
        return ResultMsg.SUCCESS(page2) ;
    }

    @Override
    public ResultMsg insertCategory(TbCategory tbCategory) {
        boolean b = this.saveOrUpdate(tbCategory);
        if (!b)
            throw new RuntimeException("添加分类失败") ;
        return ResultMsg.SUCCESS() ;
    }

    @Override
    public ResultMsg deleteCategory(int cid) {
        QueryWrapper<TbProduct> wrapper =new QueryWrapper<>() ;
        wrapper.eq("category_id",cid) ;
        productMapper.delete(wrapper);
        boolean b = this.removeById(cid);
        if (!b)
            throw new RuntimeException("删除分类失败") ;
        return ResultMsg.SUCCESS() ;
    }

    @Override
    public ResultMsg upload(MultipartFile file) {
        String s = QiNiuYunUploadUtil.uploadFile(file);
        return ResultMsg.SUCCESS(s);
    }

    @Override
    public ResultMsg findOne(int cid) {
        TbCategory tbCategory = this.getById(cid);
        if (tbCategory==null)
            throw new RuntimeException("根据id查询分类失败!");
        return ResultMsg.SUCCESS(tbCategory) ;
    }

    @Override
    public ResultMsg update(TbCategory tbCategory) {
        boolean b = this.saveOrUpdate(tbCategory);
        if (!b)
            throw new RuntimeException("更改分类失败!") ;
        return ResultMsg.SUCCESS() ;
    }

    @Override
    public ResultMsg findAllCategory() {
        List<TbCategory> categories =categoryMapper.findAll();
        return ResultMsg.SUCCESS(categories) ;
    }
}
