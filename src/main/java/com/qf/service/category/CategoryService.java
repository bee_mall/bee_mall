package com.qf.service.category;

import com.qf.entity.TbCategory;
import com.qf.util.ResultMsg;
import org.springframework.web.multipart.MultipartFile;

public interface CategoryService {
    ResultMsg findAll(int page, int limit);

    ResultMsg insertCategory(TbCategory tbCategory);

    ResultMsg deleteCategory(int cid);

    ResultMsg upload(MultipartFile file);

    ResultMsg findOne(int cid);

    ResultMsg update(TbCategory tbCategory);

    ResultMsg findAllCategory();
}
