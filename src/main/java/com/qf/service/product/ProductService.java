package com.qf.service.product;

import com.qf.entity.TbProduct;
import com.qf.util.ResultMsg;
import org.springframework.web.multipart.MultipartFile;

public interface ProductService {
    ResultMsg findAll(int page, int limit);

    ResultMsg findOne(int pid);

    ResultMsg update(TbProduct tbProduct);

    ResultMsg insert(TbProduct tbProduct);

    ResultMsg deleteById(int pid);

    ResultMsg findAllByName(int page, int limit, String productName);

    ResultMsg upload(MultipartFile file);

    ResultMsg findProductByTime();

    ResultMsg findProductByName(int page, int limit, String productName,Integer cid);

    ResultMsg findAllByCategoryId(int page, int limit, int cid);

    ResultMsg hotProd();
}
