package com.qf.service.product.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.entity.TbProduct;
import com.qf.mapper.product.ProductMapper;
import com.qf.service.product.ProductService;
import com.qf.util.ResultMsg;
import com.qf.util.product.QiNiuYunUploadUtil;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class ProductServiceImpl extends ServiceImpl<ProductMapper, TbProduct> implements ProductService {
    @Autowired
    ProductMapper productMapper ;
    @Override
    public ResultMsg findAll(int page, int limit) {
        Page<TbProduct> page1 =new Page<>(page,limit) ;
        Page<TbProduct> page2 = this.page(page1);
        return ResultMsg.SUCCESS(page2) ;
    }

    @Override
    public ResultMsg findOne(int pid) {
        TbProduct tbProduct = this.getById(pid);
        return ResultMsg.SUCCESS(tbProduct) ;
    }

    @Override
    public ResultMsg update(TbProduct tbProduct) {
        boolean b = this.saveOrUpdate(tbProduct);
        if (!b)
            throw new RuntimeException("删除商品失败");
        int count =productMapper.updateProductUpdateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()),tbProduct.getProductId());
        return ResultMsg.SUCCESS() ;
    }

    @Override
    public ResultMsg insert(TbProduct tbProduct) {
        boolean b = this.saveOrUpdate(tbProduct);
        if (!b)
            throw new RuntimeException("添加商品失败");
        String format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        productMapper.creatTime(format,tbProduct.getProductId());
        productMapper.updateTime(format,tbProduct.getProductId());
        return ResultMsg.SUCCESS() ;
    }

    @Override
    public ResultMsg deleteById(int pid) {
        boolean b = this.removeById(pid);
        if (!b)
            throw new RuntimeException("删除商品失败") ;
        return ResultMsg.SUCCESS() ;
    }

    @Override
    public ResultMsg findAllByName(int page, int limit, String productName) {
        QueryWrapper<TbProduct> wrapper =new QueryWrapper<>() ;
        wrapper.like("product_name",productName) ;
        Page<TbProduct> page1 =new Page<>(page ,limit) ;
        Page<TbProduct> page2 = this.page(page1, wrapper);
        return ResultMsg.SUCCESS(page2) ;
    }

    @Override
    public ResultMsg upload(MultipartFile file) {
        String s = QiNiuYunUploadUtil.uploadFile(file);
        return ResultMsg.SUCCESS(s) ;
    }

    @Override
    public ResultMsg findProductByTime() {
        QueryWrapper<TbProduct> wrapper =new QueryWrapper<>() ;
        wrapper.orderByDesc("create_time").last("limit 0,8") ;
        List<TbProduct> tbProducts = productMapper.selectList(wrapper);
        if (!(tbProducts.size()>0))
            throw new RuntimeException("查询最新商品失败!") ;
        return ResultMsg.SUCCESS(tbProducts) ;
    }

    @Override
    public ResultMsg findProductByName(int page, int limit, String productName, Integer cid) {
        if (productName=="" && cid!=null){
            return this.findAllByCategoryId(page, limit, cid);
        }else if (productName!=""){
            Page<TbProduct> page1 =new Page<>(page,limit) ;
            QueryWrapper<TbProduct> wrapper =new QueryWrapper<>() ;
            wrapper.like("product_name",productName);
            Page<TbProduct> page2 = this.page(page1, wrapper);
            return ResultMsg.SUCCESS(page2) ;
        }else {
            throw new RuntimeException("查询冲突") ;
        }
    }

    @Override
    public ResultMsg findAllByCategoryId(int page, int limit, int cid) {
        Page<TbProduct> page1 =new Page<>(page,limit) ;
        QueryWrapper<TbProduct> wrapper =new QueryWrapper<>() ;
        wrapper.eq("category_id",cid);
        Page<TbProduct> page2 = this.page(page1, wrapper);
        return ResultMsg.SUCCESS(page2) ;
    }

    @Override
    public ResultMsg hotProd() {
        QueryWrapper<TbProduct> wrapper =new QueryWrapper<>() ;
        wrapper.orderByDesc("sold_num").last("limit 0,8");
        List<TbProduct> tbProducts = productMapper.selectList(wrapper);
        if (!(tbProducts.size()>0))
            throw new RuntimeException("查询热门商品失败") ;
        return ResultMsg.SUCCESS(tbProducts) ;
    }


}
