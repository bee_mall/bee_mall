package com.qf.service.order.impl;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.config.AlipayConfig;
import com.qf.entity.TbOrderItem;
import com.qf.entity.TbOrders;
import com.qf.entity.TbUserAddr;
import com.qf.entity.TbUsers;
import com.qf.expection.ServiceException;
import com.qf.mapper.order.OrderItemMapper;
import com.qf.mapper.order.OrdersMapper;
import com.qf.mapper.user.UserAddrMapper;
import com.qf.mapper.user.UserMapper;
import com.qf.service.order.OrderService;
import com.qf.util.ResultMsg;
import com.qf.util.SysStatus;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * @author h'j
 * @description TODO
 * @date 2023/6/10 23:28
 */
@Service
@Transactional
public class OrderServiceImpl extends ServiceImpl<OrdersMapper, TbOrders> implements OrderService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private OrderItemMapper orderItemMapper;
    @Autowired
    private UserAddrMapper userAddrMapper;


    /**
     * 根据用户名模糊搜索订单
     * @param userName 用户名
     * @return 响应结果实体
     */
    @Override
    public ResultMsg findAllByUserName(String userName) {

        //先根据userName模糊搜寻出所有用户id user_id
        QueryWrapper<TbUsers> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("user_name", userName);
        List<TbUsers> tbUsers = userMapper.selectList(queryWrapper);
        QueryWrapper<TbOrders> tbOrdersQueryWrapper = new QueryWrapper<>();
        if(tbUsers==null || tbUsers.size()==0)
            throw new ServiceException(SysStatus.SELECT_FAIL, "暂无相关用户");
        //检索到用户信息,根据userId查询到相关订单
        List<List<TbOrders>> list = new ArrayList<>();
        for (TbUsers user : tbUsers){
            List<TbOrders> orders = this.list(tbOrdersQueryWrapper);
            list.add(orders);
        }
        return ResultMsg.SUCCESS(list);
    }

    /**
     * 分页查询所有订单
     * @param page 当前页
     * @param limit 每页条数
     * @return 响应结果实体
     */
    @Override
    public ResultMsg findAllOrders(int page, int limit, String userName) {
        Page<TbOrders> page1 = null;
        if("".equals(userName) || userName == null){
            Page<TbOrders> ordersPage = new Page<>(page, limit);
            page1 = this.page(ordersPage);
        }else{
            //根据用户名插到用户id
            QueryWrapper<TbUsers> queryWrapper = new QueryWrapper<>();
            QueryWrapper<TbUsers> user_name = queryWrapper.eq("user_name", userName);
            TbUsers tbUsers = userMapper.selectOne(user_name);
            if(tbUsers==null){
                System.out.println("测试"+tbUsers);
                throw new ServiceException(SysStatus.SELECT_FAIL, "暂无该用户");
            }
            //根据用户id查询订单并分页
            QueryWrapper<TbOrders> queryWrapper1 = new QueryWrapper<>();
            QueryWrapper<TbOrders> user_id = queryWrapper1.eq("user_id", tbUsers.getUserId());
            Page<TbOrders> ordersPage = new Page<>(page, limit);
            page1 = this.page(ordersPage, user_id);
        }
        List<TbOrders> orders = page1.getRecords();
        for(TbOrders order : orders){
            Integer userId = order.getUserId();
            //根据orderId查询到所属的用户名
            TbUsers user = userMapper.selectById(userId);
            order.setUserName(user.getUserName());
        }
        return ResultMsg.SUCCESS(page1);
    }

    /**
     * 删除单个订单
     * @param orderId 订单id
     * @return 响应结果实体
     */
    @Override
    public ResultMsg deleteOneOrder(String orderId) {
        //先根据订单查询到订单项,先删除订单项再删除订单
        QueryWrapper<TbOrderItem> queryWrapper = new QueryWrapper<>();
        QueryWrapper<TbOrderItem> order_id = queryWrapper.eq("order_id", orderId);
        int i = orderItemMapper.delete(order_id);
        //删除订单
        boolean flag = this.removeById(orderId);
        if(flag)
            return ResultMsg.SUCCESS();
        throw new ServiceException(SysStatus.DELETE_FAIL, "删除订单失败");
    }

    /**
     * 修改单个订单
     * @param order 修改后的订单实体
     * @return 响应结果实体
     */
    @Override
    public ResultMsg updateOneOrder(TbOrders order) {
        boolean flag = this.saveOrUpdate(order);
        if(flag)
            return ResultMsg.SUCCESS();
        throw new ServiceException(SysStatus.UPDATE_FAIL, "修改订单信息失败");
    }

    /**
     * 根据订单号查询订单项
     * @param orderId 订单id
     * @return 响应结果实体
     */
    @Override
    public ResultMsg findAllOrderItemByOrderId(String orderId) {
        QueryWrapper<TbOrderItem> queryWrapper = new QueryWrapper<>();
        QueryWrapper<TbOrderItem> order_id = queryWrapper.eq("order_id", orderId);
        List<TbOrderItem> tbOrderItems = orderItemMapper.selectList(order_id);
        return ResultMsg.SUCCESS(tbOrderItems);
    }

    /**
     * 根据订单项id删除订单项
     * @param itemId 订单项id
     * @return 响应结果实体
     */
    @Override
    public ResultMsg deleteOrderItem(String itemId) {
        QueryWrapper<TbOrderItem> queryWrapper = new QueryWrapper<>();
        QueryWrapper<TbOrderItem> item_id = queryWrapper.eq("item_id", itemId);
        int i = orderItemMapper.delete(item_id);
        if(i>0)
            return ResultMsg.SUCCESS();
        throw new ServiceException(SysStatus.DELETE_FAIL, "删除订单项失败");
    }

    /**
     * 修改订单项
     * @param item 修改后的订单项
     * @return 响应结果实体
     */
    @Override
    public ResultMsg updateOneOrderItem(TbOrderItem item) {
        int i = orderItemMapper.updateById(item);
        if(i>0)
            return ResultMsg.SUCCESS();
        throw new ServiceException(SysStatus.UPDATE_FAIL, "修改订单项失败");
    }

    /**
     * 根据订单编号查询单个订单
     * @param orderId 订单id
     * @return 响应结果实体
     */
    @Override
    public ResultMsg findOneOrder(String orderId) {
        TbOrders order = this.getById(orderId);
        return ResultMsg.SUCCESS(order);
    }

    /**
     * 根据订单项编号查询单个订单项
     * @param itemId 订单项id
     * @return 响应结果实体
     */
    @Override
    public ResultMsg findOneOrderItem(String itemId) {
        TbOrderItem tbOrderItem = orderItemMapper.selectById(itemId);
        return ResultMsg.SUCCESS(tbOrderItem);
    }

    /**
     * 数据导出 xlsx
     * @return
     */
    @Override
    public Workbook export() {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("订单表");
        Row row = sheet.createRow(0);
        row.createCell(0).setCellValue("订单ID");
        row.createCell(1).setCellValue("用户ID");
        row.createCell(2).setCellValue("用户名");
        row.createCell(3).setCellValue("产品名称");
        row.createCell(4).setCellValue("收货人");
        row.createCell(5).setCellValue("收货人联系电话");
        row.createCell(6).setCellValue("收货地址");
        row.createCell(7).setCellValue("订单总价格");
        row.createCell(8).setCellValue("实际支付总价格");
        row.createCell(9).setCellValue("支付方式(1:微信 2:支付宝)");
        row.createCell(10).setCellValue("订单备注");
        row.createCell(11).setCellValue("订单状态 1:待付款 2:待发货 3:待收货 4:待评价 5:已完成 6:已关闭");
        row.createCell(12).setCellValue("配送方式");
        row.createCell(13).setCellValue("物流单号");
        row.createCell(14).setCellValue("订单运费");
        row.createCell(15).setCellValue("付款时间");
        row.createCell(16).setCellValue("发货时间");
        row.createCell(17).setCellValue("完成时间");
        row.createCell(18).setCellValue("取消时间");
        //row.createCell(19).setCellValue("订单关闭类型");
        row.createCell(19).setCellValue("创建时间");
        row.createCell(20).setCellValue("更新时间");
        List<TbOrders> orders = this.list();
        for(TbOrders order : orders){
            Integer userId = order.getUserId();
            //根据orderId查询到所属的用户名
            TbUsers user = userMapper.selectById(userId);
            order.setUserName(user.getUserName());
        }
        for (int i = 0;i <orders.size();i++){
            TbOrders tbOrder = orders.get(i);
            Row r = sheet.createRow(i+1);
            r.createCell(0).setCellValue(tbOrder.getOrderId());
            r.createCell(1).setCellValue(tbOrder.getUserId());
            r.createCell(2).setCellValue(tbOrder.getUserName());
            r.createCell(3).setCellValue(tbOrder.getUntitled());
            r.createCell(4).setCellValue(tbOrder.getReceiverName());
            r.createCell(5).setCellValue(tbOrder.getReceiverMobile());
            r.createCell(6).setCellValue(tbOrder.getReceiverAddress());
            r.createCell(7).setCellValue(tbOrder.getTotalAmount());
            r.createCell(8).setCellValue(tbOrder.getActualAmount());
            r.createCell(9).setCellValue(tbOrder.getPayType());
            r.createCell(10).setCellValue(tbOrder.getOrderRemark());
            r.createCell(11).setCellValue(tbOrder.getStatus());
            r.createCell(12).setCellValue(tbOrder.getDeliveryType());
            r.createCell(13).setCellValue(tbOrder.getDeliveryFlowId());
            r.createCell(14).setCellValue(tbOrder.getOrderFreight());
            r.createCell(15).setCellValue(tbOrder.getPayTime());
            r.createCell(16).setCellValue(tbOrder.getDeliveryTime());
            r.createCell(17).setCellValue(tbOrder.getFlishTime());
            r.createCell(18).setCellValue(tbOrder.getCancelTime());
            //r.createCell(19).setCellValue(tbOrder.getCloseType());
            r.createCell(19).setCellValue(tbOrder.getCreateTime());
            r.createCell(20).setCellValue(tbOrder.getUpdateTime());
        }
        return workbook;
    }

    /**
     * 修改订单收货地址
     * @param orderId 点单编号
     * @param addrId 收货地址编号
     * @return
     */
    @Override
    public ResultMsg updateReceiverAddress(String orderId, Integer addrId) {
        //查询出用户地址信息,生成地址 receiverAddress 存入订单中
        TbUserAddr tbUserAddr = userAddrMapper.selectById(addrId);
        String receiverAddress = tbUserAddr.getProvinceName()
                +" "+tbUserAddr.getCityName()
                +" "+tbUserAddr.getAreaName()
                +" "+tbUserAddr.getAddress();

        TbOrders order = new TbOrders();
        order.setOrderId(orderId);
        order.setReceiverName(tbUserAddr.getReceiverName());
        order.setReceiverMobile(tbUserAddr.getReceiverMobile());
        order.setReceiverAddress(receiverAddress);
        //修改更新订单的时间
        order.setUpdateTime(new Date());
        boolean flag = this.updateById(order);
        if(flag) {
            TbOrders tbOrders = ordersMapper.selectById(orderId);
            return ResultMsg.SUCCESS(tbOrders);
        }
        throw new ServiceException(SysStatus.UPDATE_FAIL, "修改收货地址失败");
    }

    /**
     * 订单超时取消修改状态
     * @param orderId 订单id
     */
    @Override
    public void updateOrderStatus(String orderId) {
        //先查询订单状态是否已经支付,为支付执行修改
        QueryWrapper<TbOrders> queryWrapper = new QueryWrapper<>();
        QueryWrapper<TbOrders> order_id = queryWrapper.eq("order_id", orderId);
        TbOrders orders = ordersMapper.selectOne(order_id);
        if(orders!=null){
            //不为空,点单还存在,判断是否支付
            if(orders.getStatus().equals("1")){
                //未支付
                TbOrders order = new TbOrders();
                order.setOrderId(orderId);
                //订单状态 6 已关闭
                order.setStatus("6");
                //订单关闭类型 1 超时未支付
                order.setCloseType(1);
                //更新订单
                int i = ordersMapper.updateById(order);
                if(i==0)
                    throw new ServiceException(SysStatus.UPDATE_FAIL, "订单超时关闭异常");
            }
        }
    }

    /**
     * 根据用户信息 user_id 查询所属订单
     * @param session 域对象
     * @return 响应结果实体
     */
    @Override
    public ResultMsg findAllMyOrder(HttpSession session) {

        //获得userId 根据user_id 查询出订单
        TbUsers user = (TbUsers) session.getAttribute(SysStatus.USER_INFO);
        if(user==null)
            throw new ServiceException(SysStatus.CREATE_FAIL, "您暂未登录");
        Integer userId = user.getUserId();
        QueryWrapper<TbOrders> queryWrapper = new QueryWrapper<>();
        QueryWrapper<TbOrders> user_id = queryWrapper.eq("user_id", userId);
        List<TbOrders> orders = ordersMapper.selectList(user_id);
        if(orders == null || orders.size()==0)
            throw new ServiceException(SysStatus.SELECT_FAIL, "您暂无订单");
        //根据订单id 查询到订单项目

        for(TbOrders order : orders){
            QueryWrapper<TbOrderItem> queryWrapper1 = new QueryWrapper<>();
            QueryWrapper<TbOrderItem> order_id = queryWrapper1.eq("order_id", order.getOrderId());
            List<TbOrderItem> tbOrderItems = orderItemMapper.selectList(order_id);
            System.out.println(tbOrderItems.toString());
            order.setOrderItemList(tbOrderItems);
        }

        return ResultMsg.SUCCESS(orders);
    }

    /**
     * 用户取消订单,关闭订单
     * @param orderId 订单id
     * @return 响应结果实体
     */
    @Override
    public ResultMsg cancelMyOrder(String orderId) {
        TbOrders order = new TbOrders();
        order.setOrderId(orderId);
        order.setStatus("6");
        order.setCloseType(4);
        order.setUpdateTime(new Date());
        int i = ordersMapper.updateById(order);
        if(i>0)
            return ResultMsg.SUCCESS();
        throw new ServiceException(SysStatus.UPDATE_FAIL, "取消订单异常");
    }

    /**
     * 订单支付
     * @param orderId 订单id
     * @return 响应结果实体
     */
    @Override
    public ResultMsg alipayMyOrder(String orderId, HttpServletRequest request, HttpServletResponse response) {

        //根据id插到订单
        TbOrders order = ordersMapper.selectById(orderId);

        //PrintWriter out = response.getWriter();


        String result = null;

        try {
            //获得初始化的AlipayClient
            AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.app_id, AlipayConfig.merchant_private_key, "json", AlipayConfig.charset, AlipayConfig.alipay_public_key, AlipayConfig.sign_type);

            //设置请求参数
            AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
            alipayRequest.setReturnUrl(AlipayConfig.return_url);
            alipayRequest.setNotifyUrl(AlipayConfig.notify_url);

            alipayRequest.setBizContent("{\"out_trade_no\":\"" + order.getOrderId() + "\","   //订单编号
                    + "\"total_amount\":\"" + order.getTotalAmount() + "\","     //订单金额
                    + "\"subject\":\"" + "食品消费付款" + "\","         //订单名称
                    + "\"body\":\"" + "付款金额"+order.getTotalAmount()+"元" + "\","               // 描述
                    + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

            //请求
            result = alipayClient.pageExecute(alipayRequest).getBody();

            //输出
            //out.print(result);
            //System.out.println("===========================");

            System.out.println(result);
        } catch (AlipayApiException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //out.flush();
        //out.close();
        return ResultMsg.SUCCESS(result);
    }

    @Override
    public void alipayReturn(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //返回页面

    }

    @Override
    public void notifyUrl(HttpServletRequest request, HttpServletResponse response) throws IOException {

        System.out.println("========回调触发了吗");

        //支付宝响应post请求
        PrintWriter out = response.getWriter();
        //获取支付宝GET过来反馈信息
        Map<String, String> params = new HashMap<String, String>();
        Map<String, String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用
            System.out.println("转码前：" + valueStr);
            //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            //System.out.println("转码后："+valueStr);
            params.put(name, valueStr);
            //订单账号/订单总金额/订单中的信息主题("数码产品....")
        }


        try {
            //调用SDK验证签名 加载配置文件
            boolean signVerified = AlipaySignature.rsaCheckV1(params, AlipayConfig.alipay_public_key, AlipayConfig.charset, AlipayConfig.sign_type);

            //——请在这里编写您的程序（以下代码仅作参考）——
            if (signVerified) {
                //商户订单号
                String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"), "UTF-8");

                //支付宝交易号
                String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"), "UTF-8");

                //付款金额
                String total_amount = new String(request.getParameter("total_amount").getBytes("ISO-8859-1"), "UTF-8");

                //调用业务接口,将订单的状态变为已经支付(2 待发货), 支付方式改为支付宝 2, 支付时间pay_time now, update_time
                TbOrders orders = new TbOrders();
                orders.setOrderId(out_trade_no);
                orders.setStatus("2");
                orders.setPayType(2);
                orders.setActualAmount(Integer.parseInt(total_amount));
                orders.setPayTime(new Date());
                orders.setUpdateTime(new Date());
                int i = ordersMapper.updateById(orders);
                if (i > 0) {
                    //修改成功,订单完成,做出提示,跳转页面
                    //request.getRequestDispatcher("/WEB-INF/pay-success.html").forward(request, response);
                }

            } else {
                out.println("验签失败");
            }
            //——请在这里编写您的程序（以上代码仅作参考）——
        } catch (AlipayApiException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
