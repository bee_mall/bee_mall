package com.qf.service.order;

import com.qf.entity.TbOrderItem;
import com.qf.entity.TbOrders;
import com.qf.util.ResultMsg;
import org.apache.poi.ss.usermodel.Workbook;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author h'j
 * @description TODO
 * @date 2023/6/10 23:28
 */
public interface OrderService {

    /**
     * 根据用户名模糊搜索订单
     * @param userName 用户名
     * @return 响应结果实体
     */
    ResultMsg findAllByUserName(String userName);

    /**
     * 分页查询所有订单
     * @param page 当前页
     * @param limit 每页条数
     * @return 响应结果实体
     */
    ResultMsg findAllOrders(int page, int limit, String userName);

    /**
     * 删除单个订单
     * @param orderId 订单id
     * @return 响应结果实体
     */
    ResultMsg deleteOneOrder(String orderId);

    /**
     * 修改单个订单
     * @param order 修改后的订单实体
     * @return 响应结果实体
     */
    ResultMsg updateOneOrder(TbOrders order);

    /**
     * 根据订单号查询订单项
     * @param orderId 订单id
     * @return 响应结果实体
     */
    ResultMsg findAllOrderItemByOrderId(String orderId);

    /**
     * 根据订单项id删除订单项
     * @param itemId 订单项id
     * @return 响应结果实体
     */
    ResultMsg deleteOrderItem(String itemId);

    /**
     * 修改订单项
     * @param item 修改后的订单项
     * @return 响应结果实体
     */
    ResultMsg updateOneOrderItem(TbOrderItem item);

    /**
     * 根据订单编号查询单个订单
     * @param orderId 订单id
     * @return 响应结果实体
     */
    ResultMsg findOneOrder(String orderId);

    /**
     * 根据订单项编号查询单个订单项
     * @param itemId 订单项id
     * @return 响应结果实体
     */
    ResultMsg findOneOrderItem(String itemId);

    /**
     * 数据导出 xlsx
     * @return
     */
    Workbook export();

    /**
     * 修改订单收货地址
     * @param orderId 点单编号
     * @param addrId 收货地址编号
     * @return 响应结果实体
     */
    ResultMsg updateReceiverAddress(String orderId, Integer addrId);

    /**
     * 订单超时取消修改状态
     * @param orderId 订单id
     */
    void updateOrderStatus(String orderId);

    /**
     * 根据用户信息 user_id 查询所属订单
     * @param session 域对象
     * @return 响应结果实体
     */
    ResultMsg findAllMyOrder(HttpSession session);

    /**
     * 用户取消订单,关闭订单
     * @param orderId 订单id
     * @return 响应结果实体
     */
    ResultMsg cancelMyOrder(String orderId);

    /**
     * 订单支付
     * @param orderId 订单id
     * @return 响应结果实体
     */
    ResultMsg alipayMyOrder(String orderId, HttpServletRequest request, HttpServletResponse response);

    /**
     * 支付宝同步回调
     * @param request
     * @param response
     */
    void alipayReturn(HttpServletRequest request, HttpServletResponse response) throws IOException;

    /**
     * 支付宝异步步回调
     * @param request
     * @param response
     */
    void notifyUrl(HttpServletRequest request, HttpServletResponse response) throws IOException;
}
