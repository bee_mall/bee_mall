package com.qf.service.productdetails.impl;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.entity.ProductDetails.ShoppingCartVO;
import com.qf.entity.ProductDetails.entity.ShoppingCart;
import com.qf.entity.TbUsers;
import com.qf.expection.ServiceException;
import com.qf.mapper.productDetail.ShoppingDetailsCartMapper;
import com.qf.service.productdetails.ShopCartService;
import com.qf.util.ResultMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * ClassName: ShopCartImpl
 * Description:
 * date: 2023/6/14 18:05
 *
 * @author xiaoliu
 * @version 2021.2
 * @since JDK 1.8
 */
@Slf4j
@Service
public class ShopCartImpl extends ServiceImpl<ShoppingDetailsCartMapper, ShoppingCartVO> implements ShopCartService {
    @Autowired
    ShoppingDetailsCartMapper shoppingCartMapper;
    @Override
    public ResultMsg addShoppingCart(ShoppingCart shoppingCart, HttpSession session) {
        TbUsers user = (TbUsers) session.getAttribute("user");
        if(user==null){
            throw new ServiceException(302,"用户未登录");
        }
        shoppingCart.setUserId(user.getUserId());
        log.info("shop",shoppingCart);

        Map<String, Object> map = BeanUtil.beanToMap(shoppingCart);
        ShoppingCartVO shoppingCartVO = BeanUtil.mapToBean(map, ShoppingCartVO.class, false);
        int insert = shoppingCartMapper.insert(shoppingCartVO);

        if (insert > 0) {
            return new ResultMsg(200, "添加成功", null);
        }
        return new ResultMsg(401, "查询失败", null);
    }
}

