package com.qf.service.productdetails.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.entity.ProductDetails.TbProductVo;
import com.qf.entity.TbProductParams;
import com.qf.entity.TbProductSku;
import com.qf.mapper.parammapper.ParamMapper;
import com.qf.mapper.productDetail.ProductVoMapper;
import com.qf.mapper.skumapper.SkuMapper;
import com.qf.service.productdetails.ProductVoService;
import com.qf.util.ResultMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ClassName: ProductVoServiceImpl
 * Description:
 * date: 2023/6/13 11:14
 *
 * @author xiaoliu
 * @version 2021.2
 * @since JDK 1.8
 */
@Service
public class ProductVoServiceImpl extends ServiceImpl<ProductVoMapper, TbProductVo> implements ProductVoService {
    @Autowired
    SkuMapper skuMapper;
    @Autowired
    ParamMapper paramMapper;
    @Autowired
    ProductVoMapper productVoMapper;

    @Override
    public ResultMsg getProductDetailInfo(Integer productId) {
        TbProductVo product = this.getById(productId);
        QueryWrapper<TbProductSku> wrapper = new QueryWrapper<>();
        wrapper.eq("product_id",productId);
        List<TbProductSku> tbProductSkus = skuMapper.selectList(wrapper);
        product.setSkus(tbProductSkus);
        Map<String, Object> productMap = new HashMap<>();
        productMap.put("product",product);
        productMap.put("productSkus",tbProductSkus);
        if (productMap.size() > 0) {
            return new  ResultMsg(200,"商品详情",productMap);
        }
        return new ResultMsg(401, "查询失败", null);
    }


    @Override
    public ResultMsg getProductParamsById(Integer productId) {
        QueryWrapper<TbProductParams> wrapper = new QueryWrapper<>();
        wrapper.eq("product_id", productId);
        List<TbProductParams> productParams = paramMapper.selectList(wrapper);
        if (productParams.size() > 0) {
            return new ResultMsg(200, "查询成功", productParams.get(0));
        }
        return new ResultMsg(401, "查询失败", null);
    }


    @Override
    public ResultMsg getSkubyProductId(Integer productId) {
        QueryWrapper<TbProductSku> wrapper = new QueryWrapper<>();
        wrapper.eq("product_id", productId).eq("status",1);
        List<Map<String, Object>> maps = skuMapper.selectMaps(wrapper);
        return new ResultMsg(200,"查询成功",maps);
    }

    @Override
    public ResultMsg getProductbyProductId(Integer productId) {
        QueryWrapper<TbProductVo> wrapper = new QueryWrapper<>();
        wrapper.eq("product_id", productId).eq("product_status",1);
        List<TbProductVo> productVOS = productVoMapper.selectList(wrapper);
        return new ResultMsg(200,"查询成功",productVOS);
    }
}

