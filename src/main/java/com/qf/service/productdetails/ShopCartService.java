package com.qf.service.productdetails;

import com.qf.entity.ProductDetails.entity.ShoppingCart;
import com.qf.util.ResultMsg;

import javax.servlet.http.HttpSession;

/**
 * ClassName: ShopCartService
 * Description:
 * date: 2023/6/14 18:04
 *
 * @author xiaoliu
 * @version 2021.2
 * @since JDK 1.8
 */
public interface ShopCartService {
    /**
     * 根据用户id添加购物车
     * @param shoppingCart
     * @return
     */
    ResultMsg addShoppingCart(ShoppingCart shoppingCart, HttpSession session);
}
