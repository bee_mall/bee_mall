package com.qf.service.productdetails;

import com.qf.controller.productsku.ProductSku;
import com.qf.entity.ProductDetails.TbProductVo;
import com.qf.util.ResultMsg;

import java.util.List;

/**
 * ClassName: ProductVoService
 * Description:
 * date: 2023/6/13 10:47
 *
 * @author xiaoliu
 * @version 2021.2
 * @since JDK 1.8
 */
public interface ProductVoService {
    /**
     * 根据商品id查询当前商品的详细信息(基本信息 套餐 图片）
     *
     * @return
     */
    ResultMsg getProductDetailInfo(Integer productId);

    /**
     * 根据商品id查询当前商品的参数信息
     *
     * @return
     */
    ResultMsg getProductParamsById(Integer productId);


    /**
     * 根据商品id查询当前商品的套餐
     *
     * @return
     */
    ResultMsg getSkubyProductId(Integer productId);

    /**
     * 根据商品id查询当前商品的基本信息（名称，销量，内容）
     *
     * @return
     */
    ResultMsg getProductbyProductId(Integer productId);

}
