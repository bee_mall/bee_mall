package com.qf.service.shoppingCart;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.entity.TbShoppingCart;
import com.qf.mapper.shoppingCart.ShoppingCartMapper;
import com.qf.util.ResultMsg;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author h'j
 * @description TODO
 * @date 2023/6/13 13:58
 */
public interface ShoppingCartService {

    /**
     * 根据登录用户,返回该y用户的购物车信息
     * @param session 域对象
     * @return 响应结果对象
     */
    ResultMsg findAllCartsByUser(HttpSession session);

    /**
     * 根据购物车id删除购物车内的商品
     * @param cartId 购物车id
     * @return 响应结果对象
     */
    ResultMsg deleteOneCartsByUser(String cartId);

    /**
     * 前台发起支付,生成订单
     * @param cart 购物车信息
     * @return 响应结果对象
     */
    ResultMsg createOrder(TbShoppingCart cart, HttpSession session);

    /**
     * 查询用户地址
     * @param session 域对象
     * @return 响应结果对象
     */
    ResultMsg findAllUserAddr(HttpSession session);

    /**
     * 前台发起支付,生成订单
     * @param cartList  购物车信息集合
     * @param session 域对象
     * @return 响应结果对象
     */
    ResultMsg createOrderByCartArr(List<TbShoppingCart> cartList, HttpSession session);

    /**
     * 删除下架商品 购物车里
     * @return 响应结果对象
     */
    ResultMsg deleteCartsByDown(HttpSession session);

    /**
     * 删除选中的购物车项
     * @param cartList 购物车信息集合
     * @return 响应结果对象
     */
    ResultMsg deleteManyCarts(List<TbShoppingCart> cartList);
}
