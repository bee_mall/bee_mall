package com.qf.service.shoppingCart.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.entity.*;
import com.qf.expection.ServiceException;
import com.qf.mapper.order.OrderItemMapper;
import com.qf.mapper.order.OrdersMapper;
import com.qf.mapper.product.ProductMapper;
import com.qf.mapper.shoppingCart.ShoppingCartMapper;
import com.qf.mapper.skumapper.SkuMapper;
import com.qf.mapper.user.UserAddrMapper;
import com.qf.mapper.user.UserMapper;
import com.qf.service.shoppingCart.ShoppingCartService;
import com.qf.util.ResultMsg;
import com.qf.util.SysStatus;
import com.qf.util.UUIDUtils;
import com.qf.util.order.MyDelayMessage;
import com.qf.util.order.MyDelayQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * @author h'j
 * @description TODO
 * @date 2023/6/13 14:00
 */
@Service
public class ShoppingCartServiceImpl extends ServiceImpl<ShoppingCartMapper, TbShoppingCart> implements ShoppingCartService {

    @Autowired
    private ShoppingCartMapper shoppingCartMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private SkuMapper skuMapper;
    @Autowired
    private UserAddrMapper userAddrMapper;
    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private OrderItemMapper orderItemMapper;



    /**
     * 根据登录用户,返回该y用户的购物车信息
     * @param session 域对象
     * @return 响应结果对象
     */
    @Override
    public ResultMsg findAllCartsByUser(HttpSession session) {

        //从session中获得当前的登录用户信息
        TbUsers user = (TbUsers) session.getAttribute(SysStatus.USER_INFO);
        if(user==null)
            throw new ServiceException(SysStatus.NOT_LOGIN, "请登录后查看");
        //根据user_id查询到相关联的购物车信息
        QueryWrapper<TbShoppingCart> queryWrapper = new QueryWrapper<>();
        QueryWrapper<TbShoppingCart> user_id = queryWrapper.eq("user_id", user.getUserId());
        List<TbShoppingCart> list = this.list(user_id);
        for (TbShoppingCart cart : list){
            String skuId = cart.getSkuId();
            Integer productId = cart.getProductId();
            //根据购sku_id查询到查询到sku_name
            TbProductSku productSku = skuMapper.selectById(skuId);
            cart.setSkuName(productSku.getSkuName());
            //根据product_id查询到product_name和product_image
            TbProduct tbProduct = productMapper.selectById(productId);
            cart.setProduct(tbProduct);
        }
        return ResultMsg.SUCCESS(list);
    }

    /**
     * 根据购物车id删除购物车内的商品
     * @param cartId 购物车id
     * @return 响应结果对象
     */
    @Override
    public ResultMsg deleteOneCartsByUser(String cartId) {
        int i = shoppingCartMapper.deleteById(cartId);
        if(i>0)
            return ResultMsg.SUCCESS();
        throw new ServiceException(SysStatus.DELETE_FAIL, "移出购物车失败");
    }

    /**
     * 前台发起支付,生成订单 和 订单项
     * @param cart 购物车信息
     * @return 响应结果对象
     */
    @Override
    public ResultMsg createOrder(TbShoppingCart cart, HttpSession session) {

        //生成订单
        TbOrders order = new TbOrders();

        //生成订单号
        String orderId = UUIDUtils.getId();
        order.setOrderId(orderId);

        //生成订单项
        TbOrderItem item = new TbOrderItem();
        //生成订单项id
        String itemId = UUIDUtils.getId();
        item.setItemId(itemId);
        //订单id
        item.setOrderId(orderId);
        //商品id
        item.setProductId(cart.getProductId());
        //商品名称
        TbProduct tbProduct = productMapper.selectById(cart.getProductId());
        item.setProductName(tbProduct.getProductName());
        //商品图片
        item.setProductImg(tbProduct.getProductImage());
        //skuID
        item.setSkuId(cart.getSkuId());
        //sku名称
        item.setSkuName(cart.getSkuName());
        //商品价格
        item.setProductPrice(cart.getProductPrice());
        //购买数量
        item.setBuyCounts(Integer.parseInt(cart.getCartNum()));
        //商品总金额
        item.setTotalAmount(cart.getProductPrice()*Integer.parseInt(cart.getCartNum()));
        //加入购物车时间
        item.setBasketDate(new Date());
        //购买时间
        //评论状态： 0 未评价
        item.setIsComment(0);
        //创建时间
        item.setCreateTime(new Date());
        //更新时间
        item.setUpdateTime(new Date());
        //存入数据库
        int insert = orderItemMapper.insert(item);

        if(insert==0)
            throw new ServiceException(SysStatus.CREATE_FAIL, "创建订单项异常");

        //获得userId
        TbUsers user = (TbUsers) session.getAttribute(SysStatus.USER_INFO);
        if(user==null){
            throw new ServiceException(SysStatus.NOT_LOGIN,"暂未登录");
        }

        Integer userId = user.getUserId();
        order.setUserId(userId);

        //商品名
        order.setUntitled(cart.getProduct().getProductName());

        //根据userId查询到默认收货地址
        QueryWrapper<TbUserAddr> queryWrapper = new QueryWrapper<>();
        Map<String,Object> map = new HashMap<>();
        map.put("user_id",userId);
        map.put("common_addr",1);
        QueryWrapper<TbUserAddr> queryWrapper1 = queryWrapper.allEq(map);
        TbUserAddr tbUserAddr = userAddrMapper.selectOne(queryWrapper1);

        if(tbUserAddr==null){
            //throw new ServiceException(405,"您暂无默认收货地址");
            //没有默认地址,默认选第一条地址
            QueryWrapper<TbUserAddr> queryWrapper2 = new QueryWrapper<>();
            QueryWrapper<TbUserAddr> user_id = queryWrapper2.eq("user_id", userId);
            List<TbUserAddr> tbUserAddrs = userAddrMapper.selectList(user_id);
            if(tbUserAddrs==null || tbUserAddrs.size()==0)
                throw new ServiceException(405,"您暂无收货地址");
            tbUserAddr = tbUserAddrs.get(0);
        }

        //根据tbUserAddr获取到相关订单信息
        //收货人快照
        String receiverName = tbUserAddr.getReceiverName();
        order.setReceiverName(receiverName);

        //收货人手机号
        String receiverMobile = tbUserAddr.getReceiverMobile();
        order.setReceiverMobile(receiverMobile);

        //收货地址
        String receiverAddress = tbUserAddr.getProvinceName()
                +" "+tbUserAddr.getCityName()
                +" "+tbUserAddr.getAreaName()
                +" "+tbUserAddr.getAddress();
        order.setReceiverAddress(receiverAddress);

        //订单总价格 cart中
        double totalMoney = Integer.parseInt(cart.getCartNum()) * cart.getProductPrice();
        order.setTotalAmount(totalMoney);
        //订单状态 1:待付款
        order.setStatus("1");
        //订单运费 0 包邮
        order.setOrderFreight(0.0);
        //创建时间 createTime
        Date createTime = new Date();
        order.setCreateTime(createTime);

        //添加订单
        int i = ordersMapper.insert(order);
        if(i>0){
            //删除购物车项cart
            shoppingCartMapper.deleteById(cart.getCartId());
            MyDelayQueue.getQueue().produce(new MyDelayMessage(orderId,createTime));
            return ResultMsg.SUCCESS(order);
        }
        throw new ServiceException(SysStatus.CREATE_FAIL, "创建订单信息失败");

    }

    /**
     * 查询用户地址
     * @param session 域对象
     * @return 响应结果对象
     */
    @Override
    public ResultMsg findAllUserAddr(HttpSession session) {
        //获得userId
        TbUsers user = (TbUsers) session.getAttribute(SysStatus.USER_INFO);
        if(user==null){
            throw new ServiceException(SysStatus.NOT_LOGIN,"暂未登录");
        }
        Integer userId = user.getUserId();
        QueryWrapper<TbUserAddr> queryWrapper = new QueryWrapper<>();
        QueryWrapper<TbUserAddr> user_id = queryWrapper.eq("user_id", userId);
        List<TbUserAddr> tbUserAddrs = userAddrMapper.selectList(user_id);
        return ResultMsg.SUCCESS(tbUserAddrs);
    }

    /**
     * 前台发起支付,生成订单
     * @param cartList  购物车信息集合
     * @param session 域对象
     * @return 响应结果对象
     */
    @Override
    public ResultMsg createOrderByCartArr(List<TbShoppingCart> cartList, HttpSession session) {

        //获得userId
        TbUsers user = (TbUsers) session.getAttribute(SysStatus.USER_INFO);
        if(user==null)
            throw new ServiceException(SysStatus.CREATE_FAIL, "您暂未登录");
        Integer userId = user.getUserId();

        //生成订单
        TbOrders order = new TbOrders();

        //生成订单号
        String orderId = UUIDUtils.getId();
        order.setOrderId(orderId);

        TbOrderItem item = new TbOrderItem();

        //订单名
        StringJoiner untitledJoiner = new StringJoiner(",");

        //总价
        double totalMoney = 0;
        //生成订单项
        for(TbShoppingCart cart : cartList){
            //生成订单项id
            String itemId = UUIDUtils.getId();
            item.setItemId(itemId);
            //订单id
            item.setOrderId(orderId);
            //商品id
            item.setProductId(cart.getProductId());
            //商品名称
            TbProduct tbProduct = productMapper.selectById(cart.getProductId());
            item.setProductName(tbProduct.getProductName());
            //商品图片
            item.setProductImg(tbProduct.getProductImage());
            //skuID
            item.setSkuId(cart.getSkuId());
            //sku名称
            item.setSkuName(cart.getSkuName());
            //商品价格
            item.setProductPrice(cart.getProductPrice());
            //购买数量
            item.setBuyCounts(Integer.parseInt(cart.getCartNum()));
            //商品总金额
            item.setTotalAmount(cart.getProductPrice()*Integer.parseInt(cart.getCartNum()));
            //加入购物车时间
            item.setBasketDate(new Date());
            //购买时间
            //评论状态： 0 未评价
            item.setIsComment(0);
            //创建时间
            item.setCreateTime(new Date());
            //更新时间
            item.setUpdateTime(new Date());
            //生成订单名
            untitledJoiner.add(cart.getProduct().getProductName());
            //计算总价
            totalMoney+=Integer.parseInt(cart.getCartNum()) * cart.getProductPrice();
            //存入数据库
            int insert = orderItemMapper.insert(item);

            //删除购物车项cart
            shoppingCartMapper.deleteById(cart.getCartId());

            if(insert==0)
                throw new ServiceException(SysStatus.CREATE_FAIL, "创建订单项异常");
        }

        //商品名
        order.setUntitled(untitledJoiner.toString());
        order.setUserId(userId);
        //根据userId查询到默认收货地址
        QueryWrapper<TbUserAddr> queryWrapper = new QueryWrapper<>();
        Map<String,Object> map = new HashMap<>();
        map.put("user_id",userId);
        map.put("common_addr",1);
        QueryWrapper<TbUserAddr> queryWrapper1 = queryWrapper.allEq(map);
        TbUserAddr tbUserAddr = userAddrMapper.selectOne(queryWrapper1);
        //根据tbUserAddr获取到相关订单信息
        //收货人快照
        String receiverName = tbUserAddr.getReceiverName();
        order.setReceiverName(receiverName);

        //收货人手机号
        String receiverMobile = tbUserAddr.getReceiverMobile();
        order.setReceiverMobile(receiverMobile);

        //收货地址
        String receiverAddress = tbUserAddr.getProvinceName()
                +" "+tbUserAddr.getCityName()
                +" "+tbUserAddr.getAreaName()
                +" "+tbUserAddr.getAddress();
        order.setReceiverAddress(receiverAddress);

        //订单总价格 cart中
        order.setTotalAmount(totalMoney);
        //订单状态 1:待付款
        order.setStatus("1");
        //订单运费 0 包邮
        order.setOrderFreight(0.0);
        //创建时间 createTime
        Date createTime = new Date();
        order.setCreateTime(createTime);

        //添加订单
        int i = ordersMapper.insert(order);
        if(i>0){
            MyDelayQueue.getQueue().produce(new MyDelayMessage(orderId,createTime));
            return ResultMsg.SUCCESS(order);
        }
        throw new ServiceException(SysStatus.CREATE_FAIL, "创建订单信息失败");
    }

    /**
     * 删除下架商品 购物车里
     * @return 响应结果对象
     */
    @Override
    public ResultMsg deleteCartsByDown(HttpSession session) {
        boolean flag = false;
        //先查出所有订单的
        TbUsers user = (TbUsers) session.getAttribute(SysStatus.USER_INFO);
        if(user==null)
            throw new ServiceException(SysStatus.CREATE_FAIL, "您暂未登录");
        QueryWrapper<TbShoppingCart> queryWrapper = new QueryWrapper<>();
        QueryWrapper<TbShoppingCart> user_id = queryWrapper.eq("user_id", user.getUserId());
        List<TbShoppingCart> tbShoppingCarts = shoppingCartMapper.selectList(user_id);
        for (int i = 0; i < tbShoppingCarts.size(); i++) {
            TbShoppingCart cart = tbShoppingCarts.get(i);
            TbProduct tbProduct = productMapper.selectById(cart.getProductId());
            if(tbProduct.getProductStatus()==0){
                tbShoppingCarts.remove(i);
                flag = true;
            }
        }
        if(flag)
            return ResultMsg.SUCCESS();
        throw new ServiceException(SysStatus.DELETE_FAIL, "暂无已下架商品");
    }

    /**
     * 删除选中的购物车项
     * @param cartList 购物车信息集合
     * @return 响应结果对象
     */
    @Override
    public ResultMsg deleteManyCarts(List<TbShoppingCart> cartList) {
        for(TbShoppingCart cart : cartList){
            shoppingCartMapper.deleteById(cart.getCartId());
        }
        return ResultMsg.SUCCESS();
    }

}
