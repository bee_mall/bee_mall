package com.qf.service.user;

import com.qf.entity.TbUserAddr;
import com.qf.util.ResultMsg;

import javax.servlet.http.HttpSession;

import javax.servlet.http.HttpServletRequest;

public interface UserAddrService {
    ResultMsg findAll(int page, int limit, Integer userId,HttpServletRequest request);

    ResultMsg findOneById(Integer addr_id);

    ResultMsg updateUserAddr(TbUserAddr userAddr, HttpServletRequest request);

    ResultMsg deleteUserById(Integer addr_id);

    ResultMsg findAllByUserId(int page, int limit, HttpServletRequest request);

    ResultMsg updateStatByUserId(TbUserAddr userAddr);
}
