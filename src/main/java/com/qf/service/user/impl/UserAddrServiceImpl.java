package com.qf.service.user.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.entity.TbUserAddr;
import com.qf.entity.TbUsers;
import com.qf.mapper.user.UserAddrMapper;
import com.qf.mapper.user.UserMapper;
import com.qf.service.user.UserAddrService;
import com.qf.service.user.UserService;
import com.qf.util.ResultMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Service
public class UserAddrServiceImpl extends ServiceImpl<UserAddrMapper, TbUserAddr> implements UserAddrService {
    @Override
    public ResultMsg findAll(int page, int limit, Integer userId,HttpServletRequest request) {
        Page<TbUserAddr> page1 = new Page<>(page,limit);
        if (userId!=null){
            QueryWrapper wrapper = new QueryWrapper();
            wrapper.like("user_id",userId);
            Page page2 = this.page(page1, wrapper);
            if (page2!=null){
                return ResultMsg.SUCCESS(page2);
            }
            throw new RuntimeException("模糊查询失败!");
        }
        Page<TbUserAddr> page3 = this.page(page1);
        if (page3!=null){
            return ResultMsg.SUCCESS(page3);
        }
        throw new RuntimeException("查询失败!");
    }

    @Override
    public ResultMsg findOneById(Integer addr_id) {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("addr_id",addr_id);
        TbUserAddr userAddr = this.getOne(wrapper);
        if (userAddr!=null){
            return ResultMsg.SUCCESS(userAddr);
        }
        throw new RuntimeException("通过Id查询失败!");
    }

    @Override
    public ResultMsg updateUserAddr(TbUserAddr userAddr, HttpServletRequest request) {
        TbUsers user = (TbUsers) request.getSession().getAttribute("user");
        Date date = new Date();
        Integer userId = user.getUserId();
        Integer userId1 = userAddr.getUserId();
        if (userId1==null){
            userAddr.setUserId(userId);
            userAddr.setCreateTime(date);
        }
        userAddr.setUpdateTime(date);
        boolean b = this.saveOrUpdate(userAddr);
        if (b){
            return ResultMsg.SUCCESS("修改成功");
        }
        throw new RuntimeException("修改失败!");
    }

    @Override
    public ResultMsg deleteUserById(Integer addr_id) {
        boolean b = this.removeById(addr_id);
        if (b){
            return ResultMsg.SUCCESS("删除成功!");
        }
        throw new RuntimeException("删除失败!");
    }

    @Override
    public ResultMsg findAllByUserId(int page, int limit, HttpServletRequest request) {
        TbUsers user = (TbUsers) request.getSession().getAttribute("user");
        Integer userId = user.getUserId();
        Page<TbUserAddr> page1 = new Page<>(page,limit);
        QueryWrapper wrapper = new QueryWrapper();

        wrapper.eq("user_id",userId);
        Page page2 = this.page(page1, wrapper);
        if (page2!=null){
            return ResultMsg.SUCCESS(page2);
        }
        throw new RuntimeException("查询失败");
    }

    @Override
    public ResultMsg updateStatByUserId(TbUserAddr userAddr) {
        Integer userId = userAddr.getUserId();
        Integer addrId = userAddr.getAddrId();
        userAddr.setCommonAddr(0);
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("user_id",userId);
        boolean b = this.saveOrUpdate(userAddr);
        userAddr.setCommonAddr(1);
        wrapper.eq("addr_id",addrId);
        boolean b1 = this.saveOrUpdate(userAddr);
        if (b1){
            return ResultMsg.SUCCESS();
        }
        throw new RuntimeException("修改状态异常");
    }
}
