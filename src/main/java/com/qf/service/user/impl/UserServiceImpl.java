package com.qf.service.user.impl;

import cn.hutool.captcha.generator.RandomGenerator;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.entity.TbUsers;
import com.qf.mapper.user.UserMapper;
import com.qf.service.user.UserService;
import com.qf.util.MailSend;
import com.qf.util.ResultMsg;
import com.qf.util.UploadTools;
import com.qf.util.adminlogin.TokenUtils;
import com.qf.util.product.QiNiuYunUploadUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, TbUsers> implements UserService {
    @Autowired
    UploadTools uploadTools;
    @Autowired
    MailSend mailSend;
    @Autowired
    TokenUtils tokenUtils;
    @Override
    public ResultMsg findAll(int page, int limit, String userName) {
        Page<TbUsers> page1 = new Page<>(page, limit);
        if (userName != null) {
            QueryWrapper wrapper = new QueryWrapper();
            wrapper.like("user_name", userName);
            Page page2 = this.page(page1, wrapper);
            if (page2 != null) {
                return ResultMsg.SUCCESS(page2);
            }
            throw new RuntimeException("模糊查询失败!");
        }
        Page<TbUsers> page3 = this.page(page1);
        if (page3 != null) {
            return ResultMsg.SUCCESS(page3);
        }
        throw new RuntimeException("查询失败!");
    }

    @Override
    public ResultMsg findOneById(Integer userId) {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("user_id", userId);
        TbUsers user = this.getOne(wrapper);
        if (user != null) {
            return ResultMsg.SUCCESS(user);
        }
        throw new RuntimeException("通过Id查询失败!");
    }

    @Override
    public ResultMsg updateUser(TbUsers user,HttpServletRequest request) {
        /*TbUsers user1 = (TbUsers) request.getSession().getAttribute("user");
        Integer userId = user1.getUserId();*/
        user.setUserId(1);//假数据后期换成userId
        boolean b = this.saveOrUpdate(user);
        if (b) {
            return ResultMsg.SUCCESS("修改成功");
        }
        throw new RuntimeException("修改失败!");
    }

    @Override
    public ResultMsg deleteUserById(Integer userId) {
        boolean b = this.removeById(userId);
        if (b) {
            return ResultMsg.SUCCESS("删除成功!");
        }
        throw new RuntimeException("删除失败!");
    }

    @Override
    public Workbook export() {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("用户表");
        Row row = sheet.createRow(0);
        row.createCell(0).setCellValue("用户ID");
        row.createCell(1).setCellValue("用户名");
        row.createCell(2).setCellValue("昵称");
        row.createCell(3).setCellValue("真实姓名");
        row.createCell(4).setCellValue("手机号");
        row.createCell(5).setCellValue("邮箱");
        row.createCell(6).setCellValue("性别");
        List<TbUsers> users = this.list();
        for (int i = 0; i < users.size(); i++) {
            TbUsers user = users.get(i);
            Row r = sheet.createRow(i + 1);
            r.createCell(0).setCellValue(user.getUserId());
            r.createCell(1).setCellValue(user.getUserName());
            r.createCell(2).setCellValue(user.getNickName());
            r.createCell(3).setCellValue(user.getRealName());
            r.createCell(4).setCellValue(user.getUserMobile());
            r.createCell(5).setCellValue(user.getUserEmail());
            r.createCell(6).setCellValue(user.getUserSex());
        }
        return workbook;
    }

    @Override
    public ResultMsg uploadImg(MultipartFile file) {
        try {
            String upload = uploadTools.upload(file);
            if (upload != null) {
                return ResultMsg.SUCCESS(upload);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new RuntimeException("上传失败!");
    }

    @Override
    public ResultMsg addUser(TbUsers user, HttpServletRequest request) {
        Object obj = request.getSession().getAttribute("checkCode");
        if (obj!=null&&(obj instanceof String)){
            String checkCode = (String) obj;
            if (!user.getCode().equals(checkCode)){
                throw new RuntimeException("验证码错误!");
            }
        }
        if (obj==null){
            throw new RuntimeException("请输入验证码");
        }
        boolean b = this.saveOrUpdate(user);
        if (b){
            return ResultMsg.SUCCESS();
        }
        throw new RuntimeException("注册失败!");

    }

    @Override
    public ResultMsg sendMail(String mail, HttpServletRequest request) {
        try {
            RandomGenerator randomGenerator = new RandomGenerator(4);
            String checkCode = randomGenerator.generate();
            request.getSession().setAttribute("checkCode",checkCode);
            mailSend.send(mail,checkCode);
            return ResultMsg.SUCCESS();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("发送失败!");
    }

    @Override
    public ResultMsg findUserInfo(HttpServletRequest request) {
        TbUsers user = (TbUsers) request.getSession().getAttribute("user");
        Integer userId = user.getUserId();
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("user_id",userId);
        TbUsers one = this.getOne(wrapper);
        if (one!=null){
            return ResultMsg.SUCCESS(one);
        }
        throw new RuntimeException("获取失败!");
    }

    @Override
    public ResultMsg updatePass(HttpServletRequest request, String userPass) {
        TbUsers user = (TbUsers) request.getSession().getAttribute("user");
        userPass = userPass.substring(0,userPass.length()-1);
        user.setUserPass(userPass);
        boolean b = this.saveOrUpdate(user);
        if (b){
            return ResultMsg.SUCCESS();
        }
        throw new RuntimeException("修改密码失败");
    }

    @Override
    public ResultMsg userLogin(TbUsers user, HttpServletRequest request) {
        QueryWrapper<TbUsers> wrapper=new QueryWrapper<>();
        wrapper.eq("user_name",user.getUserName());
        TbUsers one = this.getOne(wrapper);
        if(one==null)
            throw new RuntimeException("用户不存在");
        if(!one.getUserPass().equals(user.getUserPass()))
            throw new RuntimeException("密码输入有误");
        //登录成功，记录用户信息
        HttpSession session = request.getSession();
        session.setAttribute("user",one);
        String userToken = tokenUtils.create(one.getUserName(), one.getUserPass());
        one.setUserToken(userToken);
        return ResultMsg.SUCCESS(one);
    }

    @Override
    public ResultMsg userLogout(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.removeAttribute("user");
        return ResultMsg.SUCCESS();
    }
}