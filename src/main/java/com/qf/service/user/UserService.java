package com.qf.service.user;

import com.qf.entity.TbUsers;
import com.qf.util.ResultMsg;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

public interface UserService {
    ResultMsg findAll(int page, int limit, String userName);

    ResultMsg findOneById(Integer userId);

    ResultMsg updateUser(TbUsers user,HttpServletRequest request);

    ResultMsg deleteUserById(Integer userId);

    Workbook export();

    ResultMsg uploadImg(MultipartFile file);

    ResultMsg addUser(TbUsers user, HttpServletRequest request);

    ResultMsg sendMail(String mail, HttpServletRequest request);

    ResultMsg findUserInfo(HttpServletRequest request);

    ResultMsg updatePass(HttpServletRequest request, String userPass);

    ResultMsg userLogin(TbUsers user, HttpServletRequest request);

    ResultMsg userLogout(HttpServletRequest request);
}
