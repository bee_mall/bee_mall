package com.qf.service.productsku;

import com.qf.entity.TbProductSku;
import com.qf.util.ResultMsg;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.multipart.MultipartFile;

/**
 * ClassName: ProductSku
 * Description:
 * date: 2023/6/12 14:43
 *
 * @author xiaoliu
 * @version 2021.2
 * @since JDK 1.8
 */

public interface ProductSkuService {
    ResultMsg findAll(int page, int limit, String skuname);

    ResultMsg findOne(int skuid);

    ResultMsg modify(TbProductSku productSku);

    Workbook export();

    ResultMsg upload(MultipartFile file);

    ResultMsg deleteProductSkuById(Integer skuid);
}
