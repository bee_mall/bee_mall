package com.qf.service.productsku.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.entity.TbProductSku;
import com.qf.mapper.skumapper.SkuMapper;
import com.qf.service.productsku.ProductSkuService;
import com.qf.util.ResultMsg;
import com.qf.util.UploadTools;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * ClassName: ProductSkuImpl
 * Description:
 * date: 2023/6/12 14:43
 *
 * @author xiaoliu
 * @version 2021.2
 * @since JDK 1.8
 */
@Service
public class ProductSkuImpl extends ServiceImpl<SkuMapper,TbProductSku> implements ProductSkuService {
    @Autowired
    UploadTools uploadTools;
    @Override
    public ResultMsg findAll(int page, int limit, String skuname) {
        Page<TbProductSku> page2=new Page<>(page,limit);
        if(skuname!=null) {
            QueryWrapper<TbProductSku> wrapper = new QueryWrapper<>();
            wrapper.like("param_brand", skuname);
            return  ResultMsg.SUCCESS(this.page(page2,wrapper));
        }
        return ResultMsg.SUCCESS(this.page(page2));
    }

    @Override
    public ResultMsg findOne(int skuid) {
        QueryWrapper<TbProductSku> wrapper=new QueryWrapper<>();
        wrapper.eq("sku_id",skuid);
        return ResultMsg.SUCCESS(this.getOne(wrapper));
    }

    @Override
    public ResultMsg modify(TbProductSku tbProductSku) {
        boolean b = this.saveOrUpdate(tbProductSku);
        if(b)
            return ResultMsg.SUCCESS();
        return ResultMsg.FAILD(201,"更新失败");
    }

    @Override
    public Workbook export() {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("规格参数");
        Row row = sheet.createRow(0);
        row.createCell(0).setCellValue("商品规格编号");
        row.createCell(1).setCellValue("商品规格名称");
        row.createCell(2).setCellValue("商品规格图片");
        row.createCell(3).setCellValue("商品属性");
        row.createCell(4).setCellValue("商品原价");
        row.createCell(5).setCellValue("商品销售价格");
        row.createCell(6).setCellValue("商品折扣力度");
        row.createCell(7).setCellValue("商品库存");
        row.createCell(8).setCellValue("商品规格状态");
        List<TbProductSku> tbProductSkus = this.list();
        for (int i = 0;i <tbProductSkus.size();i++){
            TbProductSku tbProductSku = tbProductSkus.get(i);
            Row r = sheet.createRow(i+1);
            r.createCell(0).setCellValue(tbProductSku.getSkuId());
            r.createCell(1).setCellValue(tbProductSku.getSkuName());
            r.createCell(2).setCellValue(tbProductSku.getSkuImg());
            r.createCell(3).setCellValue(tbProductSku.getUntitled());
            r.createCell(4).setCellValue(tbProductSku.getOriginalPrice());
            r.createCell(5).setCellValue(tbProductSku.getSellPrice());
            r.createCell(6).setCellValue(tbProductSku.getDiscounts());
            r.createCell(7).setCellValue(tbProductSku.getStock());
            r.createCell(8).setCellValue(tbProductSku.getStatus());
        }
        return workbook;
    }

    @Override
    public ResultMsg upload(MultipartFile file) {
        try {
            return   ResultMsg.SUCCESS(uploadTools.upload(file));
        } catch (Exception e) {
            throw new RuntimeException("上传失败");
        }
    }

    @Override
    public ResultMsg deleteProductSkuById(Integer skuid) {
        boolean b = this.removeById(skuid);
        if(b){
            return ResultMsg.SUCCESS();
        }
        throw new RuntimeException("删除库存失败");
    }
}

