package com.qf.service.productcomments;

import com.qf.entity.TbProductComments;
import com.qf.util.ResultMsg;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * 作者：涛
 * 时间：2023/6/10 16:12
 * 描述： 商品评价的业务接口
 */
public interface ProductCommentsService {

    /**
     *  查询所有商品评价
     */
    ResultMsg findAll(int page, int limit, String productName);

    /**
     *  查询评价的评价Id
     */
    ResultMsg findOne(int commId);

    /**
     * 添加或修改商品评价
     */
    ResultMsg modify(TbProductComments tbProductComments);

    /**
     *  通过商品评价Id删除商品的评价
     */
    ResultMsg delete(int commId);

    /**
     *pol的导出
     */
    Workbook export();

}
