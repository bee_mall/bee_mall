package com.qf.service.productcomments.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.entity.TbProductComments;
import com.qf.mapper.productcomments.ProductCommentsMapper;
import com.qf.service.productcomments.ProductCommentsService;
import com.qf.util.ResultMsg;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;


/**
 * 作者：涛
 * 时间：2023/6/10 16:13
 * 描述： 商品评价的业务接口的实现类
 */
@Service
public class ProductCommentsServiceImpl extends ServiceImpl<ProductCommentsMapper, TbProductComments> implements ProductCommentsService {

    @Autowired
    private ProductCommentsMapper productCommentsMapper;


    /**
     *  查询所有商品评价
     */
    @Override
    public ResultMsg findAll(int page, int limit, String productname) {
        Page<TbProductComments> page1 = new Page<>(page,limit);
        if (productname!=null){
            QueryWrapper<TbProductComments> queryWrapper = new QueryWrapper<>();
            queryWrapper.like("product_name",productname);
            return ResultMsg.SUCCESS(this.page(page1,queryWrapper));
        }
        return ResultMsg.SUCCESS(this.page(page1));
    }
    /**
     *  查询商品评价的评价Id
     */
    @Override
    public ResultMsg findOne(int commId) {
        QueryWrapper<TbProductComments> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("comm_id",commId);
        return ResultMsg.SUCCESS(this.getOne(queryWrapper));
    }

    /**
     * 添加或修改商品评价
     */
    @Override
    public ResultMsg modify(TbProductComments tbProductComments) {
        boolean b = this.saveOrUpdate(tbProductComments);
        if (b)
            return ResultMsg.SUCCESS();
        return ResultMsg.FAILD(201,"更新失败");
    }


    /**
     *  通过商品评价Id删除商品的评价
     */
    @Override
    public ResultMsg delete(int commId) {
        int i = productCommentsMapper.deleteById(commId);
        if (i>0) {
            return ResultMsg.FAILD(200, "删除成功");
        }else {
        return ResultMsg.FAILD(202,"删除失败");
        }
    }

    @Override
    public Workbook export() {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("商品评价表");
        Row row = sheet.createRow(0);
        row.createCell(0).setCellValue("商品评价ID");
        row.createCell(1).setCellValue("商品Id");
        row.createCell(2).setCellValue("商品名称");
        row.createCell(3).setCellValue("订单项(商品快照)ID");
        row.createCell(4).setCellValue("评论用户id");
        row.createCell(5).setCellValue("是否匿名");
        row.createCell(6).setCellValue("评价类型");
        row.createCell(7).setCellValue("评价等级");
        row.createCell(8).setCellValue("评价内容");
        row.createCell(9).setCellValue("评价晒图");
        row.createCell(10).setCellValue("评价时间");
        row.createCell(11).setCellValue("是否回复");
        row.createCell(12).setCellValue("回复内容");
        row.createCell(13).setCellValue("回复时间");
        row.createCell(14).setCellValue("是否显示");
        row.createCell(15).setCellValue("创建时间");
        row.createCell(16).setCellValue("更新时间");
        List<TbProductComments> tbProductComments = this.list();
        for (int i = 0;i<tbProductComments.size();i++){
            TbProductComments tbProductComment = tbProductComments.get(i);
            Row r = sheet.createRow(i + 1);
            r.createCell(0).setCellValue(tbProductComment.getCommId());
            r.createCell(1).setCellValue(tbProductComment.getProductId());
            r.createCell(2).setCellValue(tbProductComment.getProductName());
            r.createCell(3).setCellValue(tbProductComment.getOrderItemId());
            r.createCell(4).setCellValue(tbProductComment.getUserId());
            r.createCell(5).setCellValue(tbProductComment.getIsAnonymous());
            r.createCell(6).setCellValue(tbProductComment.getCommType());
            r.createCell(7).setCellValue(tbProductComment.getCommLevel());
            r.createCell(8).setCellValue(tbProductComment.getCommContent());
            r.createCell(9).setCellValue(tbProductComment.getCommImgs());
            r.createCell(10).setCellValue(tbProductComment.getSepcName());
            r.createCell(11).setCellValue(tbProductComment.getReplyStatus());
            r.createCell(12).setCellValue(tbProductComment.getReplyContent());
            r.createCell(13).setCellValue(tbProductComment.getReplyTime());
            r.createCell(14).setCellValue(tbProductComment.getIsShow());
            r.createCell(15).setCellValue(tbProductComment.getCreateTime());
            r.createCell(16).setCellValue(tbProductComment.getUpdateTime());
        }
        return workbook;
    }
}
