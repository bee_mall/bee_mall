package com.qf.service.productcomments.front.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.entity.TbUsers;
import com.qf.entity.productcommentsvo.ProductCommentsVO;
import com.qf.mapper.productcomments.front.ProductCommentsFrontMapper;
import com.qf.mapper.user.UserMapper;
import com.qf.service.productcomments.front.ProductCommentsFrontService;
import com.qf.util.ResultMsg;
import com.qf.util.productcomments.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * 作者：涛
 * 时间：2023/6/13 10:44
 * 描述： 前台商品评价业务接口的实现类
 */
@Service
public class ProductCommentsFrontServiceImpl extends ServiceImpl<ProductCommentsFrontMapper, ProductCommentsVO> implements ProductCommentsFrontService {

    @Autowired
    private ProductCommentsFrontMapper productCommentsFrontMapper;

    @Autowired
    private UserMapper userMapper;

    @Override
    public ResultMsg getCommentsCountByProductId(int productId) {

        //1.查询当前商品评价的总数
        QueryWrapper<ProductCommentsVO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("product_id",productId);
        int total = Math.toIntExact(productCommentsFrontMapper.selectCount(queryWrapper));

        //2.查询好评数
        queryWrapper.eq("comm_type",1);
        int goodTotal= Math.toIntExact(productCommentsFrontMapper.selectCount(queryWrapper));
        //3.查询中评数
        QueryWrapper<ProductCommentsVO> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("product_id",productId).eq("comm_type",0);
        int midTotal = Math.toIntExact(productCommentsFrontMapper.selectCount(queryWrapper1));

        //4.查询差评数
        QueryWrapper<ProductCommentsVO> queryWrapper2 = new QueryWrapper<>();
        queryWrapper2.eq("product_id",productId).eq("comm_type",-1);
        int badTotal = Math.toIntExact(productCommentsFrontMapper.selectCount(queryWrapper2));

        //计算好评率
        double percent = (Double.parseDouble(goodTotal + "") / Double.parseDouble(total + "")) * 100;
        String percentValue = (percent + "").substring(0, (percent + "").lastIndexOf(".") + 2);

        HashMap<String,Object> map = new HashMap<>();
        map.put("total",total);
        map.put("goodTotal",goodTotal);
        map.put("midTotal",midTotal);
        map.put("badTotal",badTotal);
        map.put("percent",percentValue);
        if (map.size()>0){
            return new ResultMsg(200,"查询成功",map);
        }
        return new ResultMsg(201,"查询失败",null);
    }


    @Override
    public ResultMsg listCommentsByProductId(int productId, int page, int limit) {

        //当前页数据
        int start = (page - 1) * limit;
        QueryWrapper<ProductCommentsVO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("product_id",productId).last("limit"+" "+start+","+limit);
        List<ProductCommentsVO> productCommentsVOS = productCommentsFrontMapper.selectList(queryWrapper);
        for (ProductCommentsVO productCommentsVO :productCommentsVOS){
            QueryWrapper<TbUsers> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("user_id",productCommentsVO.getUserId()).select("user_name","nick_name","user_img");
            TbUsers tbUsers = userMapper.selectOne(queryWrapper1);
            productCommentsVO.setUsername(tbUsers.getUserName());
            productCommentsVO.setNickname(tbUsers.getNickName());
            productCommentsVO.setUserImg(tbUsers.getUserImg());
        }
        //查询当前商品评价的总数
        QueryWrapper<ProductCommentsVO> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("product_id",productId);
        int total = Math.toIntExact(productCommentsFrontMapper.selectCount(queryWrapper1));

        //计算总页数
        int pageCount = total % limit == 0? total / limit : total / limit + 1;
        if (productCommentsVOS.size()>0){
            return new ResultMsg(200,"查询成功",new PageHelper<ProductCommentsVO>(total,pageCount,productCommentsVOS));
        }
        return new ResultMsg(201,"查询失败",null);

    }
}
