package com.qf.service.productcomments.front;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.entity.productcommentsvo.ProductCommentsVO;
import com.qf.util.ResultMsg;

/**
 * 作者：涛
 * 时间：2023/6/13 10:42
 * 描述：前台商品评价业务接口
 */
public interface ProductCommentsFrontService extends IService<ProductCommentsVO> {

    /**
     *  根据商品ID查询评论统计
     */
    ResultMsg getCommentsCountByProductId(int productId);

    /**
     * 根据商品id查询评论信息
     */
    ResultMsg listCommentsByProductId(int productId,int page,int limit);


}

