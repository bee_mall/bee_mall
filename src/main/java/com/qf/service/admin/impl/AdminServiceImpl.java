package com.qf.service.admin.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.entity.TbAdmin;
import com.qf.expection.adminlogin.AdminLoginException;
import com.qf.mapper.admin.AdminMapper;
import com.qf.service.admin.AdminService;
import com.qf.util.ResultMsg;
import com.qf.util.adminlogin.TokenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, TbAdmin> implements AdminService {
    @Autowired
    TokenUtils tokenUtils ;
    @Override
    public ResultMsg login(TbAdmin tbAdmin, HttpServletRequest request) {
        QueryWrapper<TbAdmin> wrapper =new QueryWrapper<>() ;
        wrapper.eq("admin_name",tbAdmin.getAdminName()) ;
        TbAdmin one = this.getOne(wrapper);
        if (one==null)
            throw new RuntimeException("该账户不存在") ;
        if (!one.getAdminPass().equals(tbAdmin.getAdminPass()))
            throw new RuntimeException("密码输入有误") ;
        request.getSession().setAttribute("admin",one);
        String token = tokenUtils.create(one.getAdminId().toString(), one.getAdminPass());
        one.setToken(token);
        return ResultMsg.SUCCESS(one) ;
    }

    @Override
    public ResultMsg update(TbAdmin tbAdmin) {
        boolean b = this.saveOrUpdate(tbAdmin);
        if (!b)
            throw new RuntimeException("修改失败");
        return ResultMsg.SUCCESS() ;
    }
}
