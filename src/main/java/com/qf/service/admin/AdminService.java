package com.qf.service.admin;

import com.qf.entity.TbAdmin;
import com.qf.util.ResultMsg;

import javax.servlet.http.HttpServletRequest;

public interface AdminService {
    ResultMsg login(TbAdmin tbAdmin, HttpServletRequest request);

    ResultMsg update(TbAdmin tbAdmin);
}
