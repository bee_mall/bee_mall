package com.qf.service.Params.paramsimpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.entity.TbProductParams;
import com.qf.mapper.parammapper.ParamMapper;
import com.qf.service.Params.ParamsService;
import com.qf.util.ResultMsg;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ClassName: paramserviceImpl
 * Description:
 * date: 2023/6/12 11:28
 *
 * @author xiaoliu
 * @version 2021.2
 * @since JDK 1.8
 */
@Service
public class paramserviceImpl extends ServiceImpl<ParamMapper, TbProductParams> implements ParamsService {
    @Override
    public ResultMsg findAll(int page, int limit, String brand) {
        Page<TbProductParams> page2=new Page<>(page,limit);
        if(brand!=null) {
            QueryWrapper<TbProductParams> wrapper = new QueryWrapper<>();
            wrapper.like("param_brand", brand);
            return  ResultMsg.SUCCESS(this.page(page2,wrapper));
        }
        return ResultMsg.SUCCESS(this.page(page2));
    }

    @Override
    public ResultMsg findOne(int paramid) {
        QueryWrapper<TbProductParams> wrapper=new QueryWrapper<>();
        wrapper.eq("param_id",paramid);
        return ResultMsg.SUCCESS(this.getOne(wrapper));
    }

    @Override
    public ResultMsg modify(TbProductParams tbProductParams) {
        boolean b = this.saveOrUpdate(tbProductParams);
        if(b)
        {return ResultMsg.SUCCESS();}

        throw new RuntimeException("更新失败");
    }

    @Override
    public Workbook export() {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("商品参数");
        Row row = sheet.createRow(0);
        row.createCell(0).setCellValue("商品参数编号");
        row.createCell(1).setCellValue("商品编号");
        row.createCell(2).setCellValue("商品产地");
        row.createCell(3).setCellValue("商品保质期");
        row.createCell(4).setCellValue("商品品牌名");
        row.createCell(5).setCellValue("商品生产厂名");
        row.createCell(6).setCellValue("商品生产厂址");
        row.createCell(7).setCellValue("商品包装方式");
        row.createCell(8).setCellValue("商品规格重量");
        row.createCell(9).setCellValue("商品存储方法");
        row.createCell(10).setCellValue("商品食用方式");
        List<TbProductParams> tbProductParams = this.list();
        for (int i = 0;i <tbProductParams.size();i++){
            TbProductParams tbProductParams1 = tbProductParams.get(i);
            Row r = sheet.createRow(i+1);
            r.createCell(0).setCellValue(tbProductParams1.getParamId());
            r.createCell(1).setCellValue(tbProductParams1.getProductId());
            r.createCell(2).setCellValue(tbProductParams1.getProductPlace());
            r.createCell(3).setCellValue(tbProductParams1.getFootPeriod());
            r.createCell(4).setCellValue(tbProductParams1.getParamBrand());
            r.createCell(5).setCellValue(tbProductParams1.getFactoryName());
            r.createCell(6).setCellValue(tbProductParams1.getFactoryAddress());
            r.createCell(7).setCellValue(tbProductParams1.getPackagingMethod());
            r.createCell(8).setCellValue(tbProductParams1.getParamWeight());
            r.createCell(9).setCellValue(tbProductParams1.getStorageMethod());
            r.createCell(10).setCellValue(tbProductParams1.getEatMethod());
        }
        return workbook;
    }

    @Override
    public ResultMsg deleteParamsById(Integer paramid) {
        boolean b = this.removeById(paramid);
        if (!b)
            throw new RuntimeException("删除参数失败") ;
        return ResultMsg.SUCCESS() ;
    }

}

