package com.qf.service.Params;

import com.qf.entity.TbProductParams;
import com.qf.util.ResultMsg;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * ClassName: ParamsService
 * Description:
 * date: 2023/6/12 11:27
 *
 * @author xiaoliu
 * @version 2021.2
 * @since JDK 1.8
 */
public interface ParamsService {
    ResultMsg findAll(int page, int limit, String brand);

    ResultMsg findOne(int paramid);

    ResultMsg modify(TbProductParams productParams);

    Workbook export();

    ResultMsg deleteParamsById(Integer paramid);
}
