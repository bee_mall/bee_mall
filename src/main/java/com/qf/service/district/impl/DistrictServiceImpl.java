package com.qf.service.district.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.entity.TbDictDistrict;
import com.qf.mapper.dirstrictmapper.DirstrictMapper;
import com.qf.service.district.DirstrictService;
import com.qf.util.ResultMsg;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ClassName: DistrictImpl
 * Description:
 * date: 2023/6/10 16:47
 *
 * @author xiaoliu
 * @version 2021.2
 * @since JDK 1.8
 */
@Service
public class DistrictServiceImpl extends ServiceImpl<DirstrictMapper, TbDictDistrict>  implements  DirstrictService{
    @Override
    public ResultMsg findAll(int page, int limit, String dictname) {
        Page<TbDictDistrict> page2=new Page<>(page,limit);
        if(dictname!=null) {
            QueryWrapper<TbDictDistrict> wrapper = new QueryWrapper<>();
            wrapper.like("dict_name", dictname);
            return  ResultMsg.SUCCESS(this.page(page2,wrapper));
        }
        return ResultMsg.SUCCESS(this.page(page2));
    }

    @Override
    public ResultMsg findOne(int dictid) {
        QueryWrapper<TbDictDistrict> wrapper=new QueryWrapper<>();
        wrapper.eq("dict_id",dictid);
        return ResultMsg.SUCCESS(this.getOne(wrapper));
    }

    @Override
    public ResultMsg modify(TbDictDistrict dictDistrict) {
        boolean b = this.saveOrUpdate(dictDistrict);
        if(b)
            return ResultMsg.SUCCESS();
        return ResultMsg.FAILD(201,"更新失败");
    }

    @Override
    public Workbook export() {
            Workbook workbook = new XSSFWorkbook();
            Sheet sheet = workbook.createSheet("地区字典");
            Row row = sheet.createRow(0);
            row.createCell(0).setCellValue("地区ID");
            row.createCell(1).setCellValue("地区父级编号");
            row.createCell(2).setCellValue("地区编码");
            row.createCell(3).setCellValue("地区区名");
            List<TbDictDistrict> tbDictDistricts = this.list();
            for (int i = 0;i <tbDictDistricts.size();i++){
                TbDictDistrict tbDictDistrict = tbDictDistricts.get(i);
                Row r = sheet.createRow(i+1);
                r.createCell(0).setCellValue(tbDictDistrict.getDictId());
                r.createCell(1).setCellValue(tbDictDistrict.getDictParent());
                r.createCell(2).setCellValue(tbDictDistrict.getDictCode());
                r.createCell(3).setCellValue(tbDictDistrict.getDictName());
            }
            return workbook;
        }

    @Override
    public ResultMsg deleteDictById(Integer dictid) {
        boolean b = this.removeById(dictid);
        if(b){
            ResultMsg.SUCCESS();
        }
        throw new  RuntimeException("删除地区失败");
    }
}

