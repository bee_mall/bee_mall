package com.qf.service.district;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.entity.TbDictDistrict;
import com.qf.mapper.dirstrictmapper.DirstrictMapper;
import com.qf.util.ResultMsg;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.multipart.MultipartFile;

/**
 * ClassName: DirstrictService
 * Description:
 * date: 2023/6/10 16:41
 *
 * @author xiaoliu
 * @version 2021.2
 * @since JDK 1.8
 */
public interface DirstrictService  {
    ResultMsg findAll(int page, int limit, String dictname);

    ResultMsg findOne(int dictid);

    ResultMsg modify(TbDictDistrict dictDistrict);

    Workbook export();

    ResultMsg deleteDictById(Integer dictid);
}

