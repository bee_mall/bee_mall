package com.qf.admininterceptor;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.qf.entity.TbAdmin;
import com.qf.expection.adminlogin.AdminLoginException;
import com.qf.mapper.admin.AdminMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component
public class AdminLoginInterceptor implements HandlerInterceptor {
    @Autowired
    AdminMapper adminMapper ;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        HttpSession session = request.getSession();
        Object admin = session.getAttribute("admin");
        if(admin==null){
            //管理员未登录直接放行,不做token校验
            return true;
        }

        String token = request.getHeader("token");
        if(!(handler instanceof HandlerMethod)){
            return true;
        }
        if (token==null)
            throw new AdminLoginException(1000,"未持有令牌") ;
        String aid = JWT.decode(token).getAudience().get(0);
        TbAdmin tbAdmin = adminMapper.selectById(Integer.parseInt(aid));
        if (tbAdmin==null)
            throw new AdminLoginException(1000,"解析token异常");
        JWTVerifier build = JWT.require(Algorithm.HMAC256(tbAdmin.getAdminPass())).build();
        try {
            build.verify(token) ;
        }catch (JWTVerificationException e){
            throw new AdminLoginException(1000,"登录信息异常,请重新登陆") ;
        }
        return true ;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
