package com.qf.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 地区字典(TbDictDistrict)实体类
 *
 * @author makejava
 * @since 2023-06-10 16:21:47
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_dict_district")
public class TbDictDistrict implements Serializable {
    private static final long serialVersionUID = -65014176130613309L;
    @TableId(type = IdType.AUTO)
    private Integer dictId;
    
    private String dictParent;
    
    private String dictCode;
    
    private String dictName;


    public Integer getDictId() {
        return dictId;
    }

    public void setDictId(Integer dictId) {
        this.dictId = dictId;
    }

    public String getDictParent() {
        return dictParent;
    }

    public void setDictParent(String dictParent) {
        this.dictParent = dictParent;
    }

    public String getDictCode() {
        return dictCode;
    }

    public void setDictCode(String dictCode) {
        this.dictCode = dictCode;
    }

    public String getDictName() {
        return dictName;
    }

    public void setDictName(String dictName) {
        this.dictName = dictName;
    }

}

