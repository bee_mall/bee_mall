package com.qf.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.io.Serializable;

/**
 * 订单项/快照 (TbOrderItem)实体类
 *
 * @author makejava
 * @since 2023-06-10 23:22:55
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_order_item")
public class TbOrderItem implements Serializable {
    private static final long serialVersionUID = -23650681298654200L;

    /**
     * 订单项ID
     */
    @TableId("item_id")
    private String itemId;
    /**
     * 订单ID
     */
    private String orderId;
    /**
     * 商品ID
     */
    private Integer productId;
    /**
     * 商品名称
     */
    private String productName;
    /**
     * 商品图片
     */
    private String productImg;
    /**
     * skuID
     */
    private String skuId;
    /**
     * sku名称
     */
    private String skuName;
    /**
     * 商品价格
     */
    private Double productPrice;
    /**
     * 购买数量
     */
    private Integer buyCounts;
    /**
     * 商品总金额
     */
    private Double totalAmount;
    /**
     * 加入购物车时间
     */
    private Date basketDate;
    /**
     * 购买时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date buyTime;
    /**
     * 评论状态： 0 未评价  1 已评价
     */
    private Integer isComment;
    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createTime;
    /**
     * 更新时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date updateTime;

}

