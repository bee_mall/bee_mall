package com.qf.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.io.Serializable;

/**
 * 商品评价 (TbProductComments)实体类
 *
 * @author makejava
 * @since 2023-06-10 15:52:33
 */
@Data
public class TbProductComments implements Serializable {
    private static final long serialVersionUID = -74080599269314880L;
    /**
     * ID
     */
    @TableId(type = IdType.AUTO)
    private Integer commId;
    /**
     * 商品id
     */
    private Integer productId;
    /**
     * 商品名称
     */
    private String productName;
    /**
     * 订单项(商品快照)ID 可为空
     */
    private String orderItemId;
    /**
     * 评论用户id 用户名须脱敏
     */
    private Integer userId;
    /**
     * 是否匿名（1:是，0:否）
     */
    private Integer isAnonymous;
    /**
     * 评价类型（1好评，0中评，-1差评）
     */
    private Integer commType;
    /**
     * 评价等级 1：好评 2：中评 3：差评
     */
    private Integer commLevel;
    /**
     * 评价内容
     */
    private String commContent;
    /**
     * 评价晒图(JSON {img1:url1,img2:url2}  )
     */
    private String commImgs;
    /**
     * 评价时间 可为空
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date sepcName;
    /**
     * 是否回复（0:未回复，1:已回复）
     */
    private Integer replyStatus;
    /**
     * 回复内容
     */
    private String replyContent;
    /**
     * 回复时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date replyTime;
    /**
     * 是否显示（1:是，0:否）
     */
    private Integer isShow;
    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /**
     * 更新时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;


}

