package com.qf.entity.productcommentsvo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 作者：涛
 * 时间：2023/6/13 14:32
 * 描述：商品评价前端实体
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_product_comments")
public class ProductCommentsVO {

    @ApiModelProperty(value = "ID")
    @TableId(value = "comm_id",type = IdType.AUTO)
    private Integer commId;


    private Integer productId;


    private String productName;


    private String orderItemId;


    private Integer userId;


    private Integer isAnonymous;


    private Integer commType;


    private Integer commLevel;


    private String commContent;


    private String commImgs;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date sepcName;


    private Integer replyStatus;


    private String replyContent;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date replyTime;


    private Integer isShow;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    //封装评论对应的用户数据
    @ApiModelProperty(value = "用户名 ")
    @TableField(value = "username" ,exist = false)
    private String username;

    @ApiModelProperty(value = "昵称")
    @TableField(value = "nickname",exist = false)
    private String nickname;

    @ApiModelProperty(value = "头像")
    @TableField(value = "user_img",exist = false)
    private String userImg;

}
