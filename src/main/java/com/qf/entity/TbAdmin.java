package com.qf.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * (TbAdmin)实体类
 *
 * @author makejava
 * @since 2023-06-14 10:32:18
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_admin")
public class TbAdmin implements Serializable {
    private static final long serialVersionUID = 684716166090216240L;
    @TableId(type = IdType.AUTO)
    private Integer adminId;
    
    private String adminName;
    
    private String adminPass;
    
    private String adminHeader;
    @TableField(exist = false)
    private String token ;

}

