package com.qf.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.io.Serializable;

/**
 * 商品分类(TbCategory)实体类
 *
 * @author makejava
 * @since 2023-06-10 17:11:02
 */
@Data
@TableName("tb_category")
public class TbCategory implements Serializable {
    private static final long serialVersionUID = -67138305580988158L;
    /**
     * 主键 分类id主键
     */
    @TableId(type = IdType.AUTO)
    private Integer categoryId;
    /**
     * 分类名称
     */
    private String categoryName;
    /**
     * 图标 logo
     */
    private String categoryIcon;
    /**
     * 口号
     */
    private String categorySlogan;


}

