package com.qf.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.util.Date;
import java.io.Serializable;

/**
 * 用户地址 (TbUserAddr)实体类
 *
 * @author makejava
 * @since 2023-06-10 15:26:38
 */
public class TbUserAddr implements Serializable {
    private static final long serialVersionUID = 328949760258960062L;
    /**
     * 地址主键id
     */
    @TableId(type = IdType.AUTO)
    private Integer addrId;
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 收件人姓名
     */
    private String receiverName;
    /**
     * 收件人手机号
     */
    private String receiverMobile;
    /**
     * 邮编
     */
    private String postCode;
    /**
     * 省-名称
     */
    private String provinceName;
    /**
     * 省-行政代号
     */
    private String provinceCode;
    /**
     * 市-名称
     */
    private String cityName;
    /**
     * 市-行政代号
     */
    private String cityCode;
    /**
     * 区-名称
     */
    private String areaName;
    /**
     * 区-行政代号
     */
    private String areaCode;
    /**
     * 详细地址
     */
    private String address;
    /**
     * 状态,1正常，0无效
     */
    private Integer status;
    /**
     * 是否默认地址 1是 1:是  0:否
     */
    private Integer commonAddr;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;


    public Integer getAddrId() {
        return addrId;
    }

    public void setAddrId(Integer addrId) {
        this.addrId = addrId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverMobile() {
        return receiverMobile;
    }

    public void setReceiverMobile(String receiverMobile) {
        this.receiverMobile = receiverMobile;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCommonAddr() {
        return commonAddr;
    }

    public void setCommonAddr(Integer commonAddr) {
        this.commonAddr = commonAddr;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

}

