package com.qf.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 商品 商品信息相关表：分类表，商品图片表，商品规格表，商品参数表(TbProduct)实体类
 *
 * @author makejava
 * @since 2023-06-10 15:42:05
 */
@Data
@TableName("tb_product")
public class TbProduct implements Serializable {
    private static final long serialVersionUID = -88197951448844779L;
    /**
     * 商品主键id
     */
    @TableId(type = IdType.AUTO)
    private Integer productId;
    /**
     * 商品名称 
     */
    private String productName;
    /**
     * 商品图片
     */
    private String productImage;
    /**
     * 分类外键id 分类id
     */
    private Integer categoryId;
    /**
     * 销量 累计销售
     */
    private Integer soldNum;
    /**
     * 默认是1，表示正常状态, -1表示删除, 0下架 默认是1，表示正常状态, -1表示删除, 0下架
     */
    private Integer productStatus;
    /**
     * 商品内容 商品内容
     */
    private String content;
    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /**
     * 更新时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;


}

