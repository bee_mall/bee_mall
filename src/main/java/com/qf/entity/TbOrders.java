package com.qf.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qf.mapper.order.OrderItemMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.beans.Transient;
import java.util.Date;
import java.io.Serializable;
import java.util.List;

/**
 * 订单 (TbOrders)实体类
 *
 * @author makejava
 * @since 2023-06-10 23:15:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_orders")
public class TbOrders implements Serializable {
    private static final long serialVersionUID = -35112894623189371L;
    /**
     * 订单ID 同时也是订单编号
     */
    @TableId("order_id")
    private String orderId;
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 产品名称（多个产品用,隔开）
     */
    private String untitled;
    /**
     * 收货人快照
     */
    private String receiverName;
    /**
     * 收货人手机号
     */
    private String receiverMobile;
    /**
     * 收货地址
     */
    private String receiverAddress;
    /**
     * 订单总价格
     */
    private Double totalAmount;
    /**
     * 实际支付总价格
     */
    private Integer actualAmount;
    /**
     * 支付方式 1:微信 2:支付宝
     */
    private Integer payType;
    /**
     * 订单备注
     */
    private String orderRemark;
    /**
     * 订单状态 1:待付款 2:待发货 3:待收货 4:待评价 5:已完成 6:已关闭
     */
    private String status;
    /**
     * 配送方式
     */
    private String deliveryType;
    /**
     * 物流单号
     */
    private String deliveryFlowId;
    /**
     * 订单运费 默认可以为零，代表包邮
     */
    private Double orderFreight;
    /**
     * 逻辑删除状态 1: 删除 0:未删除
     */
    private Integer deleteStatus;
    /**
     * 付款时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date payTime;
    /**
     * 发货时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date deliveryTime;
    /**
     * 完成时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date flishTime;
    /**
     * 取消时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date cancelTime;
    /**
     * 订单关闭类型1-超时未支付 2-退款关闭 4-买家取消 5-已通过货到付款交易
     */
    private Integer closeType;
    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /**
     * 更新时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date updateTime;

    /**
     * 用户名
     */
    @TableField(exist =false)
    private String userName;

    /**
     * 订单项集合
     */
    @TableField(exist =false)
    private List<TbOrderItem> orderItemList;

}

