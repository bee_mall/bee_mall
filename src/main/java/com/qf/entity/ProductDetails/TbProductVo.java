package com.qf.entity.ProductDetails;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qf.controller.productsku.ProductSku;
import com.qf.entity.TbProductSku;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.io.Serializable;
import java.util.List;

/**
 * 商品 商品信息相关表：分类表，商品图片表，商品规格表，商品参数表(TbProduct)实体类
 *
 * @author makejava
 * @since 2023-06-13 10:32:37
 */
@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_product")
@ApiModel(value="Product对象", description="商品 商品信息相关表：分类表，商品图片表，商品规格表，商品参数表")
public class TbProductVo implements Serializable {
    private static final long serialVersionUID = 500838742915705979L;
    /**
     * 商品主键id
     */
    @ApiModelProperty(value = "商品主键id")
    @TableId(value="product_id",type = IdType.AUTO)
    private Integer productId;
    /**
     * 商品名称 
     */
    private String productName;
    
    private String productImage;
    /**
     * 分类外键id 分类id
     */
    private Integer categoryId;
    /**
     * 销量 累计销售
     */
    private Integer soldNum;
    /**
     * 默认是1，表示正常状态, -1表示删除, 0下架 默认是1，表示正常状态, -1表示删除, 0下架
     */
    private Integer productStatus;
    /**
     * 商品内容 商品内容
     */
    private String content;
    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /**
     * 更新时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    //在查询商品的时候，关联查询商品套餐信息
    @ApiModelProperty(value = "商品套餐")
    @TableField(exist = false)
    List<TbProductSku> skus;
}

