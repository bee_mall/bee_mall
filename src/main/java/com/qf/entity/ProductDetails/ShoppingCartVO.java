package com.qf.entity.ProductDetails;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.io.Serializable;

/**
 * 购物车 (TbShoppingCart)实体类
 *
 * @author makejava
 * @since 2023-06-14 18:18:56
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_shopping_cart")
@ApiModel(value="ShoppingCart对象", description="购物车 ")
public class ShoppingCartVO implements Serializable {
    private static final long serialVersionUID = 896627465034301204L;
    /**
     * 主键
     */
    @TableId(value="cart_id",type = IdType.AUTO)
    private Integer cartId;
    /**
     * 商品ID
     */
    private Integer productId;
    /**
     * skuID
     */
    private Integer skuId;
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 购物车商品数量
     */
    private String cartNum;
    /**
     * 添加购物车时商品价格
     */
    private Double productPrice;
    /**
     * 选择的套餐的属性
     */
    private String skuProps;
    /**
     * 添加购物车时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd ")
    @JsonFormat(pattern = "yyyy-MM-dd ")
    private Date createTime;
    /**
     * 更新时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd ")
    @JsonFormat(pattern = "yyyy-MM-dd ")
    private Date updateTime;

    @TableField(value = "product_name",exist = false)
    private String productName;

    @ApiModelProperty(value = "商品主图片")
    @TableField(exist = false)
    private String productImage;

    //根据sku_id关联ProductSku查询字段
    @TableField(value = "original_price",exist = false)
    private double originalPrice;

    @TableField(value = "sell_price",exist = false)
    private double sellPrice;

    @TableField(value = "sku_name",exist = false)
    private String skuName;

    @TableField(value = "stock",exist = false)
    private int skuStock;
}

