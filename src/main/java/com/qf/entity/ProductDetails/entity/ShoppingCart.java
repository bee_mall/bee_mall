package com.qf.entity.ProductDetails.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.io.Serializable;

/**
 * 购物车 (TbShoppingCart)实体类
 *
 * @author makejava
 * @since 2023-06-14 18:32:05
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_shopping_cart")
public class ShoppingCart implements Serializable {
    private static final long serialVersionUID = -96144759260138803L;
    /**
     * 主键
     */
    @TableId(value="cart_id",type = IdType.AUTO)
    private Integer cartId;
    /**
     * 商品ID
     */
    private Integer productId;
    /**
     * skuID
     */
    private Integer skuId;
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 购物车商品数量
     */
    private String cartNum;
    /**
     * 添加购物车时商品价格
     */
    private Double productPrice;
    /**
     * 选择的套餐的属性
     */
    private String skuProps;
    /**
     * 添加购物车时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createTime;
    /**
     * 更新时间
     */

    @DateTimeFormat(pattern = "yyyy-MM-dd ")
    @JsonFormat(pattern = "yyyy-MM-dd ")
    private Date updateTime;



}

