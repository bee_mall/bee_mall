package com.qf.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.io.Serializable;

/**
 * 购物车 (TbShoppingCart)实体类
 *
 * @author makejava
 * @since 2023-06-13 13:44:19
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_shopping_cart")
public class TbShoppingCart implements Serializable {
    private static final long serialVersionUID = -43291644234214750L;
    /**
     * 主键
     */
    @TableId("cart_id")
    private Integer cartId;
    /**
     * 商品ID
     */
    private Integer productId;
    /**
     * skuID
     */
    private String skuId;
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 购物车商品数量
     */
    private String cartNum;
    /**
     * 添加购物车时商品价格
     */
    private Double productPrice;
    /**
     * 选择的套餐的属性
     */
    private String skuProps;
    /**
     * 添加购物车时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createTime;
    /**
     * 更新时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date updateTime;

    /**
     * 商品名
     */
    @TableField(exist = false)
    private TbProduct product;

    /**
     * 详细类别名sku名
     */
    @TableField(exist = false)
    private String skuName;

}

