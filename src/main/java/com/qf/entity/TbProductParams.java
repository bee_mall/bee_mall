package com.qf.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;
import java.io.Serializable;

/**
 * 商品参数 (TbProductParams)实体类
 *
 * @author makejava
 * @since 2023-06-12 15:33:37
 */
@Data
@TableName("tb_product_params")
public class TbProductParams implements Serializable {
    private static final long serialVersionUID = 204061201887169905L;
    /**
     * 商品参数id
     */
    @TableId(type = IdType.AUTO)
    private Integer paramId;
    /**
     * 商品id
     */
    private Integer productId;
    /**
     * 产地，例：中国江苏
     */
    private String productPlace;
    /**
     * 保质期，例：180天
     */
    private String footPeriod;
    /**
     * 品牌名，例：三只大灰狼
     */
    private String paramBrand;
    /**
     * 生产厂名，例：大灰狼工厂
     */
    private String factoryName;
    /**
     * 生产厂址，例：大灰狼生产基地
     */
    private String factoryAddress;
    /**
     * 包装方式 ，例：袋装
     */
    private String packagingMethod;
    /**
     * 规格重量 ，例：35g
     */
    private String paramWeight;
    /**
     * 存储方法，例：常温5~25°
     */
    private String storageMethod;
    /**
     * 食用方式，例：开袋即食
     */
    private String eatMethod;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;


    public Integer getParamId() {
        return paramId;
    }

    public void setParamId(Integer paramId) {
        this.paramId = paramId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductPlace() {
        return productPlace;
    }

    public void setProductPlace(String productPlace) {
        this.productPlace = productPlace;
    }

    public String getFootPeriod() {
        return footPeriod;
    }

    public void setFootPeriod(String footPeriod) {
        this.footPeriod = footPeriod;
    }

    public String getParamBrand() {
        return paramBrand;
    }

    public void setParamBrand(String paramBrand) {
        this.paramBrand = paramBrand;
    }

    public String getFactoryName() {
        return factoryName;
    }

    public void setFactoryName(String factoryName) {
        this.factoryName = factoryName;
    }

    public String getFactoryAddress() {
        return factoryAddress;
    }

    public void setFactoryAddress(String factoryAddress) {
        this.factoryAddress = factoryAddress;
    }

    public String getPackagingMethod() {
        return packagingMethod;
    }

    public void setPackagingMethod(String packagingMethod) {
        this.packagingMethod = packagingMethod;
    }

    public String getParamWeight() {
        return paramWeight;
    }

    public void setParamWeight(String paramWeight) {
        this.paramWeight = paramWeight;
    }

    public String getStorageMethod() {
        return storageMethod;
    }

    public void setStorageMethod(String storageMethod) {
        this.storageMethod = storageMethod;
    }

    public String getEatMethod() {
        return eatMethod;
    }

    public void setEatMethod(String eatMethod) {
        this.eatMethod = eatMethod;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

}

