package com.qf.util;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
public class MailSend {
    @Autowired
    JavaMailSender javaMailSender;
    @Value("${spring.mail.username}")
    private String fromMail;

    public void send(String toMail,String checkCode) throws MessagingException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage,true);
        mimeMessageHelper.setFrom(fromMail);
        mimeMessageHelper.setTo(toMail);
        mimeMessageHelper.setSubject("用户注册验证码");
        mimeMessageHelper.setText("<font color='blue'>"+checkCode+"</font>",true);
        javaMailSender.send(mimeMessage);
    }
}
