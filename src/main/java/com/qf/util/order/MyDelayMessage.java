package com.qf.util.order;

import lombok.Data;

import java.util.Date;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * @author h'j
 * @description TODO
 * @date 2023/6/14 8:36
 */
@Data
public class MyDelayMessage implements Delayed {

    //默认延迟10min  测试30s
    //private static final long DELAY_MS = 1000L * 60 *10;
    private static final long DELAY_MS = 1000L * 60 * 10;

    //订单id
    private final String orderId;

    //消息创建的时间戳
    private final long createTime;

    //过期时间
    private final long expire;

    //构造 由订单获取时间戳
    public MyDelayMessage(String orderId, Date createTime){
        this.orderId = orderId;
        this.createTime = createTime.getTime();
        this.expire = this.createTime + DELAY_MS;//计算出过期时长

    }

    //构造 获取当前系统时间时间
    public MyDelayMessage(String orderId,long expireSec){
        this.orderId = orderId;
        this.createTime = System.currentTimeMillis();
        this.expire = this.createTime + expireSec * 1000L;//计算出过期时长
    }

    //返回延迟时长
    @Override
    public long getDelay(TimeUnit unit) {
        //根据当前时间计算
        return unit.convert(this.expire - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
    }

    @Override
    public int compareTo(Delayed o) {
        return (int) (this.getDelay(TimeUnit.MILLISECONDS) - o.getDelay(TimeUnit.MILLISECONDS));
    }


}
