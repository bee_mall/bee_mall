package com.qf.util.order;

import com.qf.service.order.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * @author h'j
 * @description TODO
 * @date 2023/6/14 8:46
 */
@Component
@Slf4j
public class OrderCancelTask implements ApplicationRunner {

    @Autowired
    private OrderService orderService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("===>==>===>开启处理过期订单线程=========");
        new Thread(()->{
            while (true){
                MyDelayMessage msg = null;
                try {
                    msg = MyDelayQueue.getQueue().consume();
                    if(msg != null){
                        //订单过期的业务逻辑。。。。
                        log.info("=====>有订单===> " + msg.getOrderId() + " 过期");
                        orderService.updateOrderStatus(msg.getOrderId());

                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
