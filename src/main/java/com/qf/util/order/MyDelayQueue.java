package com.qf.util.order;

import lombok.Data;

import java.util.concurrent.DelayQueue;

/**
 * @author h'j
 * @description TODO
 * @date 2023/6/14 8:34
 */
@Data
public class MyDelayQueue {
    private static DelayQueue<MyDelayMessage> queue = new DelayQueue<>();

    private MyDelayQueue(){}

    private static class SingletonHolder{

        private  static MyDelayQueue singleton = new MyDelayQueue();
    }

    //单例队列
    public static MyDelayQueue getQueue(){
        return SingletonHolder.singleton;
    }


    public  Boolean  produce(MyDelayMessage message){
        return queue.add(message);
    }

   //延迟消费队列，取不到的话会阻塞一直到队列有消息再被唤醒。之后再取消息
    public  MyDelayMessage consume() throws InterruptedException {
        return queue.take();
    }


}
