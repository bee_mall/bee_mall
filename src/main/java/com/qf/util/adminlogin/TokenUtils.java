package com.qf.util.adminlogin;

import cn.hutool.core.date.DateUtil;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class TokenUtils {

    public String create(String id,String pass){
        String token = JWT.create().withAudience(id)
                .withExpiresAt(DateUtil.offsetMinute(new Date(), 10))
                .sign(Algorithm.HMAC256(pass));//盐值
        return token ;
    }
}
