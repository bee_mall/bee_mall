package com.qf.util.product;

import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

public class QiNiuYunUploadUtil {

    //构造方法私有化
    private QiNiuYunUploadUtil(){

    }

    //文件上传的方法
    public static String  uploadFile(MultipartFile multipartFile){//通过multipartFile获取到一个字节输入流
        //构造一个带指定 Region 对象的配置类
        //创建配置类,配置七牛云的区域地址
        Configuration cfg = new Configuration(Region.huadongZheJiang2());
        cfg.resumableUploadAPIVersion = Configuration.ResumableUploadAPIVersion.V2;// 指定分片上传版本
//...其他参数参考类注释

        //文件上传管理器: 需要指定上传凭证(密钥信息)
        UploadManager uploadManager = new UploadManager(cfg);
//...生成上传凭证，然后准备上传

        //七牛云上面的密钥管理信息以及存储空间名
        String accessKey = "mSOWMVaMMJfSaG56eiBdBI_UbtxPFS2cpDEjw9Dr";
        String secretKey = "hOaQB8-dVBQmkW3RESYtwcSZFhzU-LEz4XpbwB17";
        String bucket = "javaee2302";
//如果是Windows情况下，格式是 D:\\qiniu\\test.png


       // String localFilePath = "/home/qiniu/test.png";  //存储地址

        //默认不指定key的情况下，以文件内容的hash值作为文件名
        String key = null;
        //创建认证
        Auth auth = Auth.create(accessKey, secretKey);
        String upToken = auth.uploadToken(bucket); //产生"token串"代表身份信息(ak/sk)
        try {
            //获取到字节输入流
            InputStream inputStream = multipartFile.getInputStream();
            //文件上传管理器获取响应对象
            Response response = uploadManager.put(inputStream, key, upToken,null,null);
            //解析上传成功的结果
            DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
           // System.out.println(putRet.key);
            System.out.println(putRet.hash); //文件名称
            String url = "http://ruskxzzmw.bkt.clouddn.com/" ;
            return url+putRet.hash ;

        } catch (QiniuException ex) {
            Response r = ex.response;
            System.err.println(r.toString());
            try {
                System.err.println(r.bodyString());
            } catch (QiniuException ex2) {
                //ignore
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;

    }
}
