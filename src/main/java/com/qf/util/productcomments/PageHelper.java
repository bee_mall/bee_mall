package com.qf.util.productcomments;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 作者：涛
 * 时间：2023/6/13 11:56
 * 描述：
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageHelper<T> {
    //总记录数
    private int count;

    //总页数
    private int pageCount;

    //分页数据
        private List<T> list;
}
