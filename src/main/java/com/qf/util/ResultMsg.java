package com.qf.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultMsg {
    private int code;
    private String msg;
    private Object data;
    public static ResultMsg SUCCESS(){
        return new ResultMsg(200,null,null);
    }
    public static ResultMsg SUCCESS(Object data){
        return new ResultMsg(200,null,data);
    }
    public static ResultMsg SUCCESS(String msg, Object data){
        return new ResultMsg(200,msg,data);
    }

    public static ResultMsg FAILD(int code){
        return new ResultMsg(code,null,null);
    }

    public static ResultMsg FAILD(int code,String msg){
        return new ResultMsg(code,msg,null);
    }
}
