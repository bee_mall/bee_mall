package com.qf.util;

public interface SysStatus {

    Integer NOPAY = 0;

    Integer SELECT_FAIL = 401;  //查询失败

    Integer DELETE_FAIL = 402;  //删除失败

    Integer UPDATE_FAIL = 402;  //删除失败

    Integer NOT_LOGIN = 403;  //暂未登录

    Integer CREATE_FAIL = 404;  //暂未登录

    String USER_INFO = "user"; //用户登录信息

    String ADMIN_INFO = "admin_info"; //管理员登录信息

}
