package com.qf.controller.product;

import com.qf.entity.TbProduct;
import com.qf.service.product.ProductService;

import com.qf.util.ResultMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/product")
@CrossOrigin
public class ProductController {

    @Autowired
    ProductService productService ;
    @GetMapping("/findAll")
    public ResultMsg findAll(@RequestParam(required = false,defaultValue = "1") int page ,@RequestParam(required = false,defaultValue = "5") int limit ){
       return productService.findAll(page,limit);
    }
    @GetMapping("/findOne/{pid}")
    public ResultMsg findOne(@PathVariable("pid") int pid){
        return  productService.findOne(pid);
    }
    @PostMapping("/update")
    public ResultMsg update(@RequestBody TbProduct tbProduct){
       return productService.update(tbProduct);
    }
    @PostMapping("/add")
    public ResultMsg insert(@RequestBody TbProduct tbProduct){
        return productService.insert(tbProduct);
    }
    @GetMapping("/delete/{pid}")
    public ResultMsg delete(@PathVariable("pid") int pid){
        return productService.deleteById(pid);
    }
    @GetMapping("/findAllByName")
    public ResultMsg findAllByName(int page,int limit ,String productName){
        return productService.findAllByName(page,limit,productName);
    }
    @GetMapping("/findAllByCategoryId")
    public ResultMsg findAllByCategoryId(int page,int limit,int cid){
       return productService.findAllByCategoryId(page,limit,cid);
    }
    @PostMapping("/upload")
    public ResultMsg upload(MultipartFile file){
        return productService.upload(file);
    }

    /**
     * 根据上架时间 查询最新商品
     * @return 三个最新商品
     */
    @GetMapping("/newProd")
    public ResultMsg findProductByTime(){
        return productService.findProductByTime();
    }
    @GetMapping("/hotProd")
    public ResultMsg hotProd(){
        return productService.hotProd();
    }
    @GetMapping("/findProductByName")
    public ResultMsg findProductByName(int page,int limit ,String productName,Integer cid){
       return productService.findProductByName(page,limit,productName,cid);
    }

}
