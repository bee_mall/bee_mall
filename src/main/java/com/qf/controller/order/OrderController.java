package com.qf.controller.order;

import com.qf.entity.TbOrders;
import com.qf.entity.TbUsers;
import com.qf.service.order.OrderService;
import com.qf.util.ResultMsg;
import com.qf.util.SysStatus;
import io.swagger.models.auth.In;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author h'j
 * @description TODO
 * @date 2023/6/10 23:34
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    //根据用户名模糊搜索订单
    @GetMapping("/findAllByUserName/{userName}")
    public ResultMsg findAllByUserName(@PathVariable("userName") String userName){
        return orderService.findAllByUserName(userName);
    }

    @GetMapping("/findAll")
    public ResultMsg findAllOrders(int pageNum, int pageSize, String searchUserName){
        return orderService.findAllOrders(pageNum, pageSize, searchUserName);
    }

    @GetMapping("/delete/{orderId}")
    public ResultMsg deleteOneOrder(@PathVariable("orderId") String orderId){
        return orderService.deleteOneOrder(orderId);
    }

    @PostMapping("/update")
    public ResultMsg updateOneOrder(@RequestBody TbOrders order){
        return orderService.updateOneOrder(order);
    }

    @GetMapping("/update/addr")
    public ResultMsg updateReceiverAddress(String orderId, String receiverAddress){
        TbOrders tbOrders = new TbOrders();
        tbOrders.setOrderId(orderId);
        tbOrders.setReceiverAddress(receiverAddress);
        System.out.println(tbOrders);
        return orderService.updateOneOrder(tbOrders);
    }

    @GetMapping("/findOne/{orderId}")
    public ResultMsg findOneOrder(@PathVariable("orderId") String orderId){
        return orderService.findOneOrder(orderId);
    }

    @GetMapping("/export")
    public void export(HttpServletResponse response) throws IOException {
        response.setHeader("Content-Disposition", "attachment;filename=orders.xlsx");
        Workbook workbook = orderService.export();
        workbook.write(response.getOutputStream());
    }

    @GetMapping("/update/receiverAddress")
    public ResultMsg updateReceiverAddress(String orderId, Integer addrId){
        return orderService.updateReceiverAddress(orderId, addrId);
    }

    @GetMapping("/findAll/myorder")
    public ResultMsg findAllMyOrder(HttpSession session){
        return orderService.findAllMyOrder(session);
    }

    @GetMapping("/cancel/{orderId}")
    public ResultMsg cancelMyOrder(@PathVariable String orderId){
        return orderService.cancelMyOrder(orderId);
    }

    @GetMapping("/alipay/{orderId}")
    public ResultMsg alipayMyOrder(@PathVariable String orderId, HttpServletRequest request, HttpServletResponse response){
        return orderService.alipayMyOrder(orderId, request, response);
    }

    @PostMapping("/aliPayNotify")
    public void notifyUrl(HttpServletRequest request, HttpServletResponse response) throws IOException {
        orderService.notifyUrl(request, response);
    }

}
