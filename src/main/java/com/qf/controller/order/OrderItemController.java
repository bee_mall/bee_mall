package com.qf.controller.order;

import com.qf.entity.TbOrderItem;
import com.qf.service.order.OrderService;
import com.qf.util.ResultMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author h'j
 * @description TODO
 * @date 2023/6/11 9:43
 */
@RestController
@RequestMapping("/orderItem")
public class OrderItemController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/findAll/{orderId}")
    public ResultMsg findAllOrderItemByOrderId(@PathVariable("orderId") String orderId){
        return orderService.findAllOrderItemByOrderId(orderId);
    }

    @GetMapping("/delete/{itemId}")
    public ResultMsg deleteOrderItem(@PathVariable("itemId") String itemId){
        return orderService.deleteOrderItem(itemId);
    }

    @PostMapping("/update")
    public ResultMsg updateOneOrderItem(@RequestBody TbOrderItem item){
        return orderService.updateOneOrderItem(item);
    }

    @GetMapping("/findOne/{itemId}")
    public ResultMsg findOneOrderItem(@PathVariable("itemId") String itemId){
        return orderService.findOneOrderItem(itemId);
    }


}
