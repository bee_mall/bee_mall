package com.qf.controller.users;

import com.qf.entity.TbUsers;
import com.qf.service.user.UserService;
import com.qf.util.ResultMsg;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.crypto.Data;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;
    @GetMapping("/findAll")
    public ResultMsg findAll(int page,int limit,String userName){
        return userService.findAll(page,limit,userName);
    }

    @GetMapping("/findOne/{userId}")
    public ResultMsg findOne(@PathVariable Integer userId){
        return userService.findOneById(userId);
    }

    @PostMapping("/updateUser")
    public ResultMsg updateUser(@RequestBody TbUsers user,HttpServletRequest request){
        return userService.updateUser(user,request);
    }
    @GetMapping("/deleteUser/{userId}")
    public ResultMsg deleteUser(@PathVariable Integer userId){
        return userService.deleteUserById(userId);
    }
    @GetMapping("/export")
    public void export(HttpServletResponse response) throws IOException {
        response.setHeader("Content-Disposition", "attachment;filename=users.xlsx");
        Workbook workbook = userService.export();
        workbook.write(response.getOutputStream());
    }

    @PostMapping("/uploadImg")
    public ResultMsg uploadImg(MultipartFile file){
        System.out.println(file);
        return userService.uploadImg(file);
    }

    @PostMapping("/addUser")
    public ResultMsg addUser(@RequestBody TbUsers user, HttpServletRequest request){
        Date date = new Date();
        user.setCreateTime(date);
        return userService.addUser(user,request);
    }

    @GetMapping("/mail")
    public ResultMsg sendMail(String mail,HttpServletRequest request){
        return userService.sendMail(mail,request);
    }

    @GetMapping("/findUserInfo")
    public ResultMsg findUser(HttpServletRequest request){
        return userService.findUserInfo(request);
    }
    @PostMapping("/updatePass")
    public ResultMsg updatePass(HttpServletRequest request,@RequestBody String userPass){
        return userService.updatePass(request,userPass);
    }
    @PostMapping("/userLogin")
    public ResultMsg userLogin(@RequestBody TbUsers user,HttpServletRequest request){
        return userService.userLogin(user,request);
    }

    @GetMapping("/logout")
    public ResultMsg userLogout(HttpServletRequest request){
        return userService.userLogout(request);
    }
}
