package com.qf.controller.users;

import com.qf.entity.TbUserAddr;
import com.qf.entity.TbUsers;
import com.qf.service.user.UserAddrService;
import com.qf.service.user.UserService;
import com.qf.util.ResultMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/userAddr")
public class UserAddrController {
    @Autowired
    UserAddrService userAddrService;
    @GetMapping("/findAll")
    public ResultMsg findAll(int page, int limit, Integer userId,HttpServletRequest request){
        return userAddrService.findAll(page,limit,userId,request);
    }

    @GetMapping("/findAllByUserId")
    public ResultMsg findAllByUserId(int page, int limit,HttpServletRequest request){
        return userAddrService.findAllByUserId(page,limit,request);
    }
    @GetMapping("/findOne/{addr_id}")
    public ResultMsg findOne(@PathVariable Integer addr_id){
        return userAddrService.findOneById(addr_id);
    }

    @PostMapping("/updateUserAddr")
    public ResultMsg updateUser(@RequestBody TbUserAddr userAddr, HttpServletRequest request){
        return userAddrService.updateUserAddr(userAddr,request);
    }
    @GetMapping("/deleteUserAddr/{addr_id}")
    public ResultMsg deleteUserAddr(@PathVariable Integer addr_id){
        return userAddrService.deleteUserById(addr_id);
    }

    @PostMapping("/updateStatByUserId/")
    public ResultMsg updateStatByUserId(@RequestBody TbUserAddr userAddr){
        return userAddrService.updateStatByUserId(userAddr);
    }
}
