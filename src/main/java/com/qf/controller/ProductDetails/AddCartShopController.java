package com.qf.controller.ProductDetails;

import com.qf.entity.ProductDetails.entity.ShoppingCart;
import com.qf.service.productdetails.ShopCartService;
import com.qf.util.ResultMsg;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * ClassName: AddCartShopController
 * Description:
 * date: 2023/6/14 17:59
 *
 * @author xiaoliu
 * @version 2021.2
 * @since JDK 1.8
 */
@RestController
@RequestMapping("/shopcart")
public class AddCartShopController {

    @Autowired
    ShopCartService shopCartService;
    @PostMapping("/add")
    @ApiOperation("添加购物车")
    public ResultMsg addShoppingCart(@RequestBody ShoppingCart cart, HttpSession session){
        return shopCartService.addShoppingCart(cart,session);
       /* , @RequestHeader("token")String token (按需添加)*/
    }
}

