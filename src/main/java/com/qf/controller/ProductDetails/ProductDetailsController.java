package com.qf.controller.ProductDetails;

import com.qf.service.productdetails.ProductVoService;
import com.qf.util.ResultMsg;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * ClassName: ProductDetailsController
 * Description:
 * date: 2023/6/13 11:23
 *
 * @author xiaoliu
 * @version 2021.2
 * @since JDK 1.8
 */
@RestController
@RequestMapping("/product")
@Api(value = "提供商品信息相关的接口",tags = "商品管理")
public class ProductDetailsController {
    @Autowired
    ProductVoService productVoService;

    @ApiOperation("查询商品详细信息")
    @GetMapping("/detail-info/{pid}")
    public ResultMsg getProductDetailInfo(@PathVariable("pid") Integer pid){
       return productVoService.getProductDetailInfo(pid);
    }
    @ApiOperation("查询商品套餐信息")
    @GetMapping("/detail-sku/{pid}")
    public ResultMsg getProductSku(@PathVariable("pid") Integer pid){
        return productVoService.getSkubyProductId(pid);
    }
    @ApiOperation("查询商品参数信息")
    @GetMapping("/detail-params/{pid}")
    public ResultMsg getProductParams(@PathVariable("pid") Integer pid){
        return productVoService.getProductParamsById(pid);
    }
}

