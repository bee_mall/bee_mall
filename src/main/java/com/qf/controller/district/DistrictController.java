package com.qf.controller.district;

import com.qf.entity.TbDictDistrict;
import com.qf.service.district.DirstrictService;
import com.qf.util.ResultMsg;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * ClassName: DistrictController
 * Description:
 * date: 2023/6/10 16:14
 *
 * @author xiaoliu
 * @version 2021.2
 * @since JDK 1.8
 */
@CrossOrigin(allowCredentials = "true",origins = "http://localhost:8080",allowedHeaders="*")
@RestController
@RequestMapping("/district")
public class DistrictController {

    @Autowired
    DirstrictService dirstrictService;
    @GetMapping("/findAll")
    public ResultMsg findAll(int page,int limit,String dictname){
        return dirstrictService.findAll(page,limit,dictname);
    }
    @GetMapping("/{dictid}")
    public ResultMsg findOne(@PathVariable int dictid){
        return dirstrictService.findOne(dictid);
    }

    @PostMapping("/modify")
    public ResultMsg modify(@RequestBody TbDictDistrict dictDistrict){
        return dirstrictService.modify(dictDistrict);
    }
    @GetMapping("/export")
    public void export(HttpServletResponse response) throws IOException {
        response.setHeader("Content-Disposition", "attachment;filename=district.xlsx");
        Workbook workbook = dirstrictService.export();
        workbook.write(response.getOutputStream());
    }
    @GetMapping("/delete/{dictid}")
    public ResultMsg delete(@PathVariable("dictid") int dictid){
        return dirstrictService.deleteDictById(dictid);
    }
}

