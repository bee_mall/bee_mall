package com.qf.controller.productcomments;

import com.qf.entity.TbProductComments;
import com.qf.service.productcomments.ProductCommentsService;
import com.qf.util.ResultMsg;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * 作者：涛
 * 时间：2023/6/10 16:59
 * 描述：商品评价后台控制器
 */
@RestController
@RequestMapping("/productComments")
//@CrossOrigin(allowCredentials = "true",origins = "http://localhost:8080",allowedHeaders = "*")
public class ProductCommentsController {

    @Autowired
    private ProductCommentsService productCommentsService;

    /**
     *查询所有的商品评价以及通过商品名称查询商品评价
     */
    @GetMapping("/findAll")
    public ResultMsg findAllProductComments(int page, int limit, String productname){
        return productCommentsService.findAll(page,limit,productname);
    }

    /**
     *查询商品评价的评价Id
     */
    @GetMapping("/{commId}")
    public ResultMsg findOneProductComments(@PathVariable("commId")int commId){
        return productCommentsService.findOne(commId);
    }

    /**
     * 添加或修改商品评价
     */
    @PostMapping("/modify")
    public ResultMsg modifyProductComments(@RequestBody TbProductComments tbProductComments){
        return productCommentsService.modify(tbProductComments);
    }

    /**
     *  通过商品评价Id删除商品的评价
     */
    @GetMapping("/delete/{commId}")
    public ResultMsg deleteProductCommentsById(@PathVariable("commId")int commId){
        return productCommentsService.delete(commId);
    }
    /**
     *pol的导出
     */
    @GetMapping("/export")
    public void export(HttpServletResponse response) throws IOException {
        response.setHeader("Content-Disposition", "attachment;filename=productComments.xlsx");
        Workbook workbook = productCommentsService.export();
        workbook.write(response.getOutputStream());
    }
}
