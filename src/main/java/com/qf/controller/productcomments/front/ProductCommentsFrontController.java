package com.qf.controller.productcomments.front;

import com.qf.service.productcomments.front.ProductCommentsFrontService;
import com.qf.util.ResultMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 作者：涛
 * 时间：2023/6/13 12:00
 * 描述：商品评价前台控制器
 */
@RestController
@RequestMapping("/productCommentsFront")
public class ProductCommentsFrontController {

    @Autowired
    private ProductCommentsFrontService productCommentsFrontService;

    /**
     * 根据商品id查询评论信息
     */
    @GetMapping("/findAll")
    public ResultMsg getProductCommonts(int productId, int page, int limit){
        return productCommentsFrontService.listCommentsByProductId(productId,page,limit);
    }

    /**
     *  根据商品ID查询评论统计
     */
    @GetMapping("/findProductById/{productId}")
    public ResultMsg getProductCommontsCount(@PathVariable("productId") int productId){
        return productCommentsFrontService.getCommentsCountByProductId(productId);
    }

}
