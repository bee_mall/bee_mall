package com.qf.controller.shoppingCart;

import com.qf.entity.TbShoppingCart;
import com.qf.entity.TbUsers;
import com.qf.mapper.shoppingCart.ShoppingCartMapper;
import com.qf.service.shoppingCart.ShoppingCartService;
import com.qf.service.user.UserAddrService;
import com.qf.util.ResultMsg;
import com.qf.util.SysStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author h'j
 * @description TODO
 * @date 2023/6/13 13:54
 */
@RestController
@RequestMapping("/cart")
public class ShoppingCartController {

    @Autowired
    private ShoppingCartService shoppingCartService;

    @GetMapping("/findAllByUser")
    public ResultMsg findAllCartsByUser(HttpSession session){
        return shoppingCartService.findAllCartsByUser(session);
    }

    @GetMapping("/delete/{cartId}")
    public ResultMsg deleteOneCartsByUser(@PathVariable("cartId") String cartId){
        return shoppingCartService.deleteOneCartsByUser(cartId);
    }

    @PostMapping("/add")
    public ResultMsg createOrder(@RequestBody TbShoppingCart cart, HttpSession session){
        return shoppingCartService.createOrder(cart, session);
    }

    @GetMapping("/findAll/userAddr")
    public ResultMsg findAllUserAddr(HttpSession session){
        return shoppingCartService.findAllUserAddr(session);
    }

    @PostMapping("/adds")
    public ResultMsg createOrderByCartArr(@RequestBody List<TbShoppingCart> cartList, HttpSession session){
        return shoppingCartService.createOrderByCartArr(cartList, session);
    }

    @GetMapping("/delete/down")
    public ResultMsg deleteCartsByDown(HttpSession session){
        return shoppingCartService.deleteCartsByDown(session);
    }

    @PostMapping("/delete/many")
    public ResultMsg deleteManyCarts(@RequestBody List<TbShoppingCart> cartList){
        return shoppingCartService.deleteManyCarts(cartList);
    }

}
