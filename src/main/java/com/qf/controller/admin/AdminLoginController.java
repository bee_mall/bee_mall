package com.qf.controller.admin;

import com.qf.entity.TbAdmin;
import com.qf.service.admin.AdminService;
import com.qf.util.ResultMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RequestMapping("/admin")
@RestController
public class AdminLoginController {
    @Autowired
    AdminService adminService ;
    @PostMapping("/login")
    public ResultMsg login(@RequestBody TbAdmin tbAdmin,HttpServletRequest request){
       return adminService.login(tbAdmin,request);
    }
    @PostMapping("/update")
    public ResultMsg update(@RequestBody TbAdmin tbAdmin){
        return adminService.update(tbAdmin);
    }

    @GetMapping("/logout")
    public ResultMsg logout(HttpSession session){
        session.removeAttribute("admin");
        return ResultMsg.SUCCESS();
    }

}
