package com.qf.controller.productsku;

import com.qf.entity.TbProductSku;
import com.qf.service.productsku.ProductSkuService;
import com.qf.util.ResultMsg;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * ClassName: ProductSku
 * Description:
 * date: 2023/6/12 14:32
 *
 * @author xiaoliu
 * @version 2021.2
 * @since JDK 1.8
 */
@CrossOrigin(allowCredentials = "true",origins = "http://localhost:8080",allowedHeaders="*")
@RestController
@RequestMapping("/productsku")

public class ProductSku {
    @Autowired
    ProductSkuService productSkuService;
    @GetMapping("/findAll")
    public ResultMsg findAll(int page, int limit, String skuname){
        return productSkuService.findAll(page,limit,skuname);
    }
    @GetMapping("/{skuid}")
    public ResultMsg findOne(@PathVariable int skuid){
        return productSkuService.findOne(skuid);
    }

    @PostMapping("/modify")
    public ResultMsg modify(@RequestBody TbProductSku productSku){
        return productSkuService.modify(productSku);
    }
    @GetMapping("/export")
    public void export(HttpServletResponse response) throws IOException {
        response.setHeader("Content-Disposition", "attachment;filename=productsku.xlsx");
        Workbook workbook = productSkuService.export();
        workbook.write(response.getOutputStream());
    }
    @GetMapping("/delete/{skuid}")
    public ResultMsg delete(@PathVariable("skuid") int skuid){
        return productSkuService.deleteProductSkuById(skuid);
    }

    @PostMapping("/upload")
    public ResultMsg upload(MultipartFile file){
        return productSkuService.upload(file);
    }
}


