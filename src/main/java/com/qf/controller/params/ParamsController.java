package com.qf.controller.params;

import com.qf.entity.TbProductParams;
import com.qf.service.Params.ParamsService;
import com.qf.util.ResultMsg;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * ClassName: ParamsController
 * Description:
 * date: 2023/6/12 8:51
 *
 * @author xiaoliu
 * @version 2021.2
 * @since JDK 1.8
 */
@CrossOrigin(allowCredentials = "true",origins = "http://localhost:8080",allowedHeaders="*")
@RestController
@RequestMapping("/params")
public class ParamsController {
    @Autowired
    ParamsService paramsService;
    @GetMapping("/findAll")
    public ResultMsg findAll(int page, int limit, String brand){
        return paramsService.findAll(page,limit,brand);
    }
    @GetMapping("/{pid}")
    public ResultMsg findOne(@PathVariable int pid){
        return paramsService.findOne(pid);
    }

    @PostMapping("/modify")
    public ResultMsg modify(@RequestBody TbProductParams productParams){
        return paramsService.modify(productParams);
    }
    @GetMapping("/export")
    public void export(HttpServletResponse response) throws IOException {
        response.setHeader("Content-Disposition", "attachment;filename=params.xlsx");
        Workbook workbook = paramsService.export();
        workbook.write(response.getOutputStream());
    }
    @GetMapping("/delete/{paramid}")
    public ResultMsg delete(@PathVariable("paramid") int paramid){
        return paramsService.deleteParamsById(paramid);
    }
}

