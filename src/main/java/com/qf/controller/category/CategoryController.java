package com.qf.controller.category;

import com.qf.entity.TbCategory;
import com.qf.service.category.CategoryService;
import com.qf.util.ResultMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    CategoryService categoryService ;
    @GetMapping("/findAll")
    public ResultMsg findAll(int page ,int limit){
        return categoryService.findAll(page,limit);
    }
    @GetMapping("/findAllCategory")
    public ResultMsg findAllCategory(){
        return categoryService.findAllCategory();
    }

    @PostMapping("/insert")
    public ResultMsg insertCategory(@RequestBody TbCategory tbCategory){
       return categoryService.insertCategory(tbCategory);
    }

    @GetMapping("/delete/{cid}")
    public ResultMsg deleteCategory(@PathVariable("cid") int cid){
        return categoryService.deleteCategory(cid);
    }
    @PostMapping("/upload")
    public ResultMsg upload(MultipartFile file){
       return categoryService.upload(file);
    }
    @GetMapping("/findOne/{cid}")
    public ResultMsg findOne(@PathVariable("cid") int cid){
        return categoryService.findOne(cid);
    }
    @PostMapping("/update")
    public ResultMsg update(@RequestBody TbCategory tbCategory){
        return categoryService.update(tbCategory);
    }
}
