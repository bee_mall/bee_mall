package com.qf;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * ClassName: bee_mall_after
 * Description:
 * date: 2023/6/10 14:07
 *
 * @author xiaoliu
 * @version 2021.2
 * @since JDK 1.8
 */
@SpringBootApplication
public class bee_mall_after {
    public static void main(String[] args) {
        SpringApplication.run(bee_mall_after.class);
    }
}

