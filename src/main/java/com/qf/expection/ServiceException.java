package com.qf.expection;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @author h'j
 * @description TODO
 * @date 2023/6/2 20:56
 */
@Data
@Component
public class ServiceException extends RuntimeException{

    private Integer code;

    public ServiceException(){}

    public ServiceException(Integer code, String message) {
        super(message);
        this.code = code;
    }
}
