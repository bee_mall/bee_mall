package com.qf.expection;

import com.qf.util.ResultMsg;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@Component
@ResponseBody
public class UserException {

    @ExceptionHandler(Exception.class)
    public ResultMsg resultMsg(Exception e){
        e.printStackTrace();
        return ResultMsg.FAILD(777,e.getMessage());
    }
}
