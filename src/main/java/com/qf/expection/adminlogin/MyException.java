package com.qf.expection.adminlogin;

import com.qf.util.ResultMsg;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@Component
@ResponseBody
public class MyException {

    @ExceptionHandler(AdminLoginException.class)
    public ResultMsg resultMsg(AdminLoginException e){
        return ResultMsg.FAILD(e.getCode(),e.getMessage());
    }
}
