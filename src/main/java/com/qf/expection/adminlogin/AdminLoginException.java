package com.qf.expection.adminlogin;

import lombok.Data;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.stereotype.Component;

@Data
@Component
public class AdminLoginException extends RuntimeException{
    private int code ;
    public AdminLoginException(){}
    public AdminLoginException(int code ,String msg){
        super(msg);
        this.code=code ;
    }
}
