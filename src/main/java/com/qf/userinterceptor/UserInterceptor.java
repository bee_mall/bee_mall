package com.qf.userinterceptor;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qf.entity.TbUsers;
import com.qf.expection.adminlogin.AdminLoginException;
import com.qf.mapper.user.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component
public class UserInterceptor implements HandlerInterceptor {
    @Autowired
    UserMapper userMapper;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        HttpSession session = request.getSession();
        TbUsers user1 = (TbUsers) session.getAttribute("user");
        if(user1==null){
            //管理员未登录直接放行,不做token校验
            return true;
        }
        String userToken = request.getHeader("userToken");
        if(!(handler instanceof HandlerMethod)){
            return true;
        }
        if (userToken==null){
            throw new RuntimeException("未持有令牌");
        }
        String userName = JWT.decode(userToken).getAudience().get(0);
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("user_name",userName);
        TbUsers user = userMapper.selectOne(wrapper);
        if (user==null)
            throw new RuntimeException("解析token异常");
        JWTVerifier build = JWT.require(Algorithm.HMAC256(user.getUserPass())).build();
        try {
            build.verify(userToken);
        } catch (JWTVerificationException e) {
            throw new RuntimeException("登录信息异常");
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
